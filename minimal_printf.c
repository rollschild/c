#include <stdarg.h>
#include <stdio.h>

void
minimal_printf(char* fmt, ...) {
    va_list arg_ptr;
    char* fmt_ptr;
    int int_val;
    char* str_val;
    double double_val;

    va_start(arg_ptr, fmt);
    for (fmt_ptr = fmt; *fmt_ptr; fmt_ptr++) {
        if (*fmt_ptr != '%') {
            // not a formatted one
            putchar(*fmt_ptr);
            continue;
        }
        switch (*++fmt_ptr) {
        case 'd':
            // each va_arg() returns on argument and steps arg_ptr to the next
            int_val = va_arg(arg_ptr, int);
            printf("%d", int_val);
            break;
        case 'f':
            double_val = va_arg(arg_ptr, double);
            printf("%f", double_val);
            break;
        case 's':
            str_val = va_arg(arg_ptr, char*);
            for (; *str_val; str_val++) {
                putchar(*str_val);
            }
            break;
        default:
            putchar(*fmt_ptr);
            break;
        }
    }
    va_end(arg_ptr);

    return;
}

int
main(int argc, char** argv) {
    minimal_printf("my name is %s %s and I'm %d years old.\n", "Guangchu",
                   "Shi", 27);
    minimal_printf("I'm %f meters tall.\n", 1.84);

    return 0;
}

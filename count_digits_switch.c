#include <stdio.h>

int main() {
    int c = 0;
    int white_count = 0;
    int other_count = 0;
    int digits_dict[10];
    for (int i = 0; i < 10; ++i) {
        digits_dict[i] = 0;
    }

    while ((c = getchar()) != EOF) {
        switch (c) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            digits_dict[c - '0']++;
            break;
        case '\n':
        case ' ':
        case '\t':
            ++white_count;
            break;
        default:
            ++other_count;
            break;
        }
    }

    printf("digits from 0 - 9: ");
    for (int j = 0; j < 10; ++j) {
        printf("%d ", digits_dict[j]);
    }
    printf("number of white spaces: %d, and number of other characters: %d\n",
           white_count, other_count);

    return 0;
}

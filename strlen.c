#include <stddef.h>
#include <stdio.h>

#ifdef strlen
#undef strlen
#endif

int strlen(char s[]) {
    int len = 0;
    while (s[len] != '\0') {
        ++len;
    }

    return len;
}

ptrdiff_t str_len(char *str) {
    char *ptr = str;
    while (*ptr != '\0')
        ++ptr;
    return ptr - str;
}

int main() {
    char str[] = "this is a string..";
    printf("length of str is %d.\n", strlen(str));
    printf("length of str, using pointer, is %ld.\n", str_len(str));

    return 0;
}

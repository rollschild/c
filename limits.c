#include <stdio.h>
#include <limits.h>

int main() {
    printf("Max of int is: %d.\n", INT_MAX);        // 32 bits
    printf("Max of long int is: %ld.\n", LONG_MAX); // 64 bits
}

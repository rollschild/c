#include <stdio.h>

// compare two strings
// based on value, not length

int strcmp_arr(char *str1, char *str2) {
    int i;
    for (i = 0; str1[i] == str2[i]; ++i) {
        if (str1[i] == '\0') {
            return 0;
        }
    }
    return str1[i] - str2[i];
}

int strcmp_ptr(char *str1, char *str2) {
    for (; *str1 == *str2; ++str1, ++str2) {
        if (*str1 == '\0') {
            return 0;
        }
    }
    return *str1 - *str2;
}

int main() {
    char *str1 = "abcde";
    char *str2 = "abcdEf";
    printf("from strcmp_arr: %s is %s %s.\n", str1,
           strcmp_arr(str1, str2) == 0
               ? "equal to"
               : strcmp_arr(str1, str2) < 0 ? "less than" : "larger than",
           str2);
    printf("from strcmp_ptr: %s is %s %s.\n", str1,
           strcmp_ptr(str1, str2) == 0
               ? "equal to"
               : strcmp_ptr(str1, str2) < 0 ? "less than" : "larger than",
           str2);

    return 0;
}

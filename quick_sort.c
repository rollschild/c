#include <stdio.h>

void quick_sort(int arr[], int left, int right) {
    if (left >= right)
        return;
    int pos = 0;
    void swap(int[], int, int);
    int pivot = (left + right) / 2;
    swap(arr, left, pivot);
    pivot = left;
    for (pos = pivot + 1; pos <= right; ++pos) {
        if (arr[pos] < arr[left]) {
            swap(arr, ++pivot, pos);
        }
    }
    swap(arr, pivot, left);
    quick_sort(arr, left, pivot - 1);
    quick_sort(arr, pivot + 1, right);
}

void swap(int arr[], int l, int r) {
    int tmp = arr[l];
    arr[l] = arr[r];
    arr[r] = tmp;
}

int main() {
    int arr[] = {12, -2, 0, 37, 9};

    quick_sort(arr, 0, 5);

    for (int i = 0; i < 5; ++i) {
        printf("%d ", arr[i]);
    }
    printf("\n");

    return 0;
}

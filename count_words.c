#include <stdio.h>

#define IN 1
#define OUT 0

int main() {
    int state = OUT;
    int c = 0;
    int line_count, word_count, char_count;
    line_count = word_count = char_count = 0;

    while ((c = getchar()) != EOF) {
        ++char_count;

        if (c == '\n')
            ++line_count;
        if (c == '\n' || c == '\t' || c == '\b' || c == ' ') {
            state = OUT;
        } else {
            if (state == OUT) {
                state = IN;
                ++word_count;
            }
        }
    }

    printf("%d characters, %d words, and %d lines.\n", char_count, word_count,
           line_count);
}

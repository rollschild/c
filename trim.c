#include <stdio.h>
#include <string.h>

int trim(char str[]) {
    int len;
    for (len = strlen(str) - 1; len >= 0; --len) {
        if (str[len] != ' ' && str[len] != '\t' && str[len] != '\n')
            break;
    }
    str[len + 1] = '\0';
    return len;
}

int main() {
    /** char str[] = "hey    "; */
    char str[0] = "";
    int num = trim(str);
    printf("%s, %d\n", str, num);
    return 0;
}

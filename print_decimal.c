#include <stdio.h>

void print_decimal(int num) {
    if (num < 0) {
        putchar('-');
        num = -num;
    }
    if (num / 10) {
        print_decimal(num / 10);
    }
    putchar(num % 10 + '0');

    return;
}

int main() {
    int val = -2037;
    print_decimal(val);

    return 0;
}

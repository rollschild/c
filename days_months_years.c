#include <stdio.h>

static int day_dictionary[2][12] = {
    {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
};

int day_of_year(int year, int month, int day) {
    int leap = (year % 4 == 0 && year % 100 == 0) || (year % 400 == 0);

    for (int i = 0; i < month - 1; ++i) {
        day += day_dictionary[leap][i];
    }

    return day;
}

int main() {
    int year = 2019;
    int month = 6;
    int day = 24;

    printf("%02d/%02d/%04d is the %dth day of the year.\n", day, month, year,
           day_of_year(year, month, day));

    return 0;
}

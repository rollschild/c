#include <stdio.h>

void strcat_impl(char[], char[]);

int main() {
    char str_one[13] = "hello";
    char str_two[8] = ", honey";
    strcat_impl(str_one, str_two);

    printf("%s\n", str_one);

    return 0;
}

void strcat_impl(char str1[], char str2[]) {
    int i = 0, j = 0;

    while (str1[i] != '\0') {
        ++i;
    }

    while ((str1[i++] = str2[j++]) != '\0') {
        ;
    }

    return;
}

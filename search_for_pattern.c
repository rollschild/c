#include <stdio.h>
#define MAXLINE 999

int get_line(char str[], int max);
int strindex(char str[], char pattern[]);

int main() {
    char pattern[] = "llo";
    char str[MAXLINE];
    int found = 0;

    while (get_line(str, MAXLINE) > 0) {
        if (strindex(str, pattern) >= 0) {
            printf("%s", str);
            ++found;
        }
    }

    return found;
}

int get_line(char str[], int limit) {
    int c = 0;
    int pos = 0;

    while (--limit > 0 && (c = getchar()) != EOF && c != '\n') {
        str[pos] = c;
        ++pos;
    }
    if (c == '\n') {
        str[pos++] = c;
    }
    str[pos] = '\0';

    return pos;
}

int strindex(char str[], char pattern[]) {
    int pos, str_pos, pattern_pos;

    for (pos = 0; str[pos] != '\0'; ++pos) {
        for (str_pos = pos, pattern_pos = 0;
             str[str_pos] == pattern[pattern_pos]; ++str_pos, ++pattern_pos) {
            ;
        }
        if (pattern_pos > 0 && pattern[pattern_pos] == '\0') {
            return pos;
        }
    }
    return -1;
}

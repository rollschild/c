#include <ctype.h>
#include <stdio.h>

#ifndef SIZE
#define SIZE 256
#endif

#ifndef INVALID
#define INVALID 0;
#endif

int get_char(void);
void unget_char(int);
int get_int(int *);

int buffer[SIZE];
int buffer_pos;

int main() {
    int pos;
    int arr[SIZE];
    for (int j = 0; j < SIZE; ++j) {
        arr[j] = 0;
    }
    for (pos = 0; pos < SIZE && get_int(&arr[pos]) != EOF; ++pos)
        ;

    for (int i = 0; i < SIZE; ++i) {
        printf("%d ", arr[i]);
    }
    printf("\n");

    return 0;
}

int get_int(int *ptr) {
    int c;
    int sign;
    while (isspace(c = get_char()))
        ;

    if (!isdigit(c) && c != EOF && c != '+' && c != '-') {
        // invalid input
        unget_char(c);
        return INVALID;
    }

    sign = (c == '-') ? -1 : 1;
    if (c == '-' || c == '+') {
        c = get_char();
    }

    // now c is the first char that represents an actual digit
    for (*ptr = 0; isdigit(c); c = get_char()) {
        *ptr = 10 * (*ptr) + (c - '0');
    }
    *ptr *= sign;

    if (c != EOF) {
        unget_char(c);
    }

    return c;
}

int get_char(void) {
    if (buffer_pos > 0) {
        return buffer[--buffer_pos];
    } else {
        return getchar();
    }
}

void unget_char(int c) {
    if (buffer_pos == SIZE) {
        printf("Max limit of buffer reached!");
    } else {
        buffer[buffer_pos++] = c;
    }
}

#include <ctype.h>
#include <stdio.h>

#include "calc.h"

#include "./get_char.c"

/** int get_char(void);
 * void unget_char(int); */

// ONLY handles non-negatvie operands right now I believe
int get_op(char str[]) {
    // returns a string
    int c;

    while ((str[0] = c = get_char()) == ' ' || c == '\t')
        ;
    /** str[1] = '\0'; */
    if (!isdigit(c) && c != '.') {
        // encounters an operator
        return c;
    }

    int pos = 0;
    if (isdigit(c)) {
        while (isdigit(str[++pos] = c = get_char()))
            ;
    }
    // now c is NOT digit
    if (c == '.') {
        while (isdigit(str[++pos] = c = get_char()))
            ;
    }
    str[pos] = '\0';
    // now we come to the end
    if (c != EOF) {
        unget_char(c);
    }

    return NUMBER;
}

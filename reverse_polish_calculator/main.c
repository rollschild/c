#include <stdio.h>
#include <stdlib.h>

#include "calc.h"

int main() {
    // a calculator
    char input[MAX];
    int type;
    double op_rhs;

    while ((type = get_op(input)) != EOF) {
        switch (type) {
        case NUMBER:
            push(atof(input));
            break;
        case '+':
            push(pop() + pop());
            break;
        case '*':
            push(pop() * pop());
            break;
        case '-':
            op_rhs = pop();
            push(pop() - op_rhs);
            break;
        case '/':
            op_rhs = pop();
            push(pop() / op_rhs);
            break;
        case '\n':
            printf("result is %.8g\n", pop());
            break;
        default:
            printf("ERROR: unknown command.\n");
            break;
        }
    }

    return 0;
}

#include <stdio.h>

#include "calc.h"

static int buff_pos = 0;
static char buffer[MAX];

int get_char() { return buff_pos > 0 ? buffer[--buff_pos] : getchar(); }

void unget_char(int c) {
    if (buff_pos >= MAX)
        printf("max limit read characters!\n");
    else
        buffer[buff_pos++] = c;
    return;
}

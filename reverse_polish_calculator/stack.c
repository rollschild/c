#include <stdio.h>

#include "calc.h"

static double stack[MAX];
static int stack_pos = 0;

void push(double num) {
    if (stack_pos < MAX) {
        stack[stack_pos++] = num;
    } else {
        printf("maximum limit of stack reached!\n");
    }
}

double pop(void) {
    if (stack_pos <= 0) {
        printf("Nothing to pop!");
        return 0.0;
    } else {
        return stack[--stack_pos];
    }
}

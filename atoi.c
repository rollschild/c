#include <stdio.h>

int atoi(char[]);

int main() {
    char str[] = "2037";
    printf("coverted to integer: %d.\n", atoi(str));
    return 0;
}

int atoi(char str[]) {
    int pos, num = 0;
    for (pos = 0; str[pos] >= '0' && str[pos] <= '9'; ++pos) {
        num = num * 10 + (str[pos] - '0');
    }
    return num;
}

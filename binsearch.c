#include <stdio.h>

int binsearch(int, int[], int);

int main() {
    int arr[] = {-6, 0, 7, 8, 9, 12, 2037};
    int num = 12;
    int pos = binsearch(num, arr, 7);
    if (pos >= 0) {
        printf("Number %d does exist in array and position is %d!\n", num, pos);
    } else {
        printf("Number %d does NOT exist at all!", num);
    }
    return 0;
}

int binsearch(int num, int arr[], int len) {
    int low = 0, high = len - 1;
    int mid = 0;
    while(low <= high) {
        mid = (low + high) / 2;
        if (num < arr[mid]) {
            high = mid - 1;
        } else if (num > arr[mid]) {
            low = mid + 1;
        } else {
            return mid;
        }
    }
    return -1;
}

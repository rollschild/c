#include <stdio.h>

unsigned getbits(unsigned, int, int); // right-justed

int main() {
    unsigned num = 0b110100;
    unsigned res = getbits(num, 4, 3);
    printf("%d\n", res);

    return 0;
}

unsigned getbits(unsigned num, int p, int n) {
    return (num >> (p + 1 - n)) & ~(~0 << n);
}

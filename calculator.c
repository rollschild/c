#include <ctype.h>
#include <stdio.h>

#define MAXLINE 99

double a_to_f(char str[]) {
    int pos;
    double val;
    for (pos = 0; isspace(str[pos]); ++pos) {
        ;
    } // skip leading zeros

    int sign = (str[pos] == '-') ? -1 : 1;

    if (str[pos] == '-' || str[pos] == '+') {
        ++pos;
    }

    // Now pos is the first numeric digit
    for (val = 0.0; isdigit(str[pos]); ++pos) {
        val = val * 10.0 + (str[pos] - '0');
    }
    if (str[pos] == '.') {
        ++pos;
    }
    double power = 1.0;
    for (; isdigit(str[pos]); ++pos) {
        val = val * 10.0 + (str[pos] - '0');
        power *= 10.0;
    }

    val = sign * val / power;
    printf("%s => %f", str, val);
    return val;
}

int get_line(char str[], int limit) {
    // return length maybe?
    int pos = 0;
    int c;
    while (--limit > 0 && (c = getchar()) != EOF && c != '\n') {
        str[pos++] = c;
    }
    if (c == '\n') {
        str[pos++] = '\0';
    }
    /** str[pos] = '\0'; */

    return pos;
}

int main() {
    double sum, a_to_f(char[]);
    int get_line(char[], int);
    char line[MAXLINE];

    sum = 0.0;
    while (get_line(line, MAXLINE) > 0) {
        printf("\t%f\n", sum += a_to_f(line));
    }

    return 0;
}

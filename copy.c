#include <stdio.h>
#include <unistd.h>

int
main(int argc, char** argv) {
    char buf[BUFSIZ];
    int num;

    while ((num = read(0, buf, BUFSIZ)) > 0) {
        write(1, buf, num);
    }

    return 0;
}

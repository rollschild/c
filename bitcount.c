#include <stdio.h>

unsigned bitcount(unsigned);

int main() {
    // unsigned char c = getchar();
    unsigned num = 113;
    printf("%d non-zero bits.\n", bitcount(num));

    return 0;
}

unsigned bitcount(unsigned num) {
    int count;
    for (count = 0; num != 0; num >>= 1) {
        if (num & 0x1)
            ++count;
    }

    return count;
}

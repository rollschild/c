# Everything About the C Programming Language

## Memory Model

- Compiler explorer links:
  - https://godbolt.org/z/oYzdro4cY
- _By definition_ there are three distinct types that use one byte of memory:
  - `char`
  - `unsigned char`
  - `signed char`
- `sizeof(char)` is `1` _by definition_
- _every_ object `A` can be viewed as `unsigned char[sizeof A]`
- use `char` for characters and string data
- use `unsigned char` as the atom of all object types
- `sizeof` - counts how many `unsigned char`s the object occupies
- Two syntaxes for `sizeof`
  - for objects - `sizeof(obj)` or `sizeof obj`
  - for objects with type `T` - `sizeof(T)`
- `UCHAR_MAX` and `CHAR_BIT` in `<climits>` (or `<limits.h>`)
- **aliasing**: accessing the same object through different pointers
  - `register`
  - `restrict`
- `restrict` - type qualifier in C
  - `__restrict` or `__restrict__` in C++
- **Effective Type**
  - objects must be accessed
    - through their effective types, or
    - through pointer to a character type (`unsigned char*`)
  - for `union` types, this rule can be relaxed
- Variables and compound literals must be accessed:
  - through their declared type, or
  - through a pointer to a character type
- **Bus Error**
  - Two causes:
    - **non-existent address**
    - **unaligned memory access**
- `alignof()` from `<stdalign.h>` - alignment of a specific type
  - or, `_Alignof()`
- `alignas()` from `<stdalign.h>`
  - or, `_Alignas()`

## Storage

- **lifetime**
- **dynamic allocation**
  - creates storage instance that are _only_ seen as byte arrays and do _not_ have any interpretation as objects

### `malloc` and others

- in `<stdlib.h>`
- `double* ptr = malloc(len * sizeof *ptr);`
- `double* ptr = malloc(sizeof(double[len]));`
- **Do _NOT_ cast the return of `malloc` (and its friends)**
  - especially when forgetting to `#include <stdlib.h>`
  - older C compilers thinks `malloc` returns `int` and triggers the wrong conversion `int` -> pointer type
- storage allocated through `malloc` is uninitialized and has no type
- `malloc()` indicates an allocation failure by returning null pointer
- `free()` does nothing if receiving a null pointer as argument
- `realloc()`
  - does _NOT_ guarantee the returned pointer is the same as the original pointer
  - regardless, the object is considered to be a _new_ one
  - all pointers derived from the original object will be invalid
- `memcpy` vs. `memmove`
  - `memmove` does more checks
  - UB in `memcpy(dest, src)` if objects `dest` and `src` point to _overlap_

### Storage Duration

- **shadowing**
  - the visibility of an identifier can be **shadowed** by an identifier with the same name in a _subordinate_ scope
- `extern`
  - file-level storage duration
- **Every _definition_ of a variable creates a new, distinct object**
- **Read-only object literals _may_ overlap**
  ```c
  /**
  * These 4 pointers `c`, `d`, `f`, `g` may all be initialized to the same address
  * of _one_ `char` array;
  * Compiler may even save more memory: this address _may_ just be `&e[3]`
  */
  char const* c = "end";
  char const* d = "end";
  char const* e = "friend";
  char const* f = (char const[]){'e', 'n', 'd', '\0'};
  char const* g = (char const[]){'e', 'n', 'd', '\0'};
  ```
- **Storage Duration**s
  - **static** - determined at compile time
  - **automatic** - automatically determined at runtime
  - **allocated** - explicitly determined by `malloc()` and others
    - lifetime starts from `malloc()`, `calloc()`, `realloc()`, or `aligned_alloc()`
    - lifetime ends with:
      - `free` or `realloc()` that destroy it
      - or end of program execution
  - **thread** - bound to a certain thread of execution
- `extern` may refer to identifiers with external _or_ internal **linkage**
  - in addition to compiler, an identifier with linkage is usually managed by another external program, **linker**
- static **storage duration** is _NOT_ same as declaring a var with storage class `static`
- storage class `static` - internal linkage
  - can be declared in file scope (global) or in block scope (local)

#### Static Storage Duration

- lifetime of the entire program execution
- considered alive _before_ any application code is executed
- can _only_ be initialized by expressions known at compile time or can be resolved by system's process startup procedure
- Can be defined in two ways:
  - objects _defined_ in file scope
    - variables
    - compound literals
  - variables declared inside function block with storage class specifier `static`
- **Objects with static storage duration are _always_ initialized**
- Objects with lifetime of the entire program execution aren't necessarily visible in the entire program

#### Automatic Storage Duration

- Three cases:
  - any block-scope variables that are not `static`, that are declared as `auto` (default) or `register`
  - block-scope compound literals
  - temporary objects returned from function calls
- Recursion:
  - _Each_ recursion call creates a new local instance of an automatic object
- Good for compiler to optimize
- `&` is _NOT_ allowed for `register`
  - variables declared with `register` can _NOT_ alias
- **Declare local variables that are not arrays in performance-critical code as `register`**
- temporary lifetime
  - if the return type _contains_ an array type
  - objects with temporary lifetime are _readonly_
  - temporary lifetime ends at the end of the enclosing full expression (_NOT_ block!)

#### Initialization

- the storage duration of an object determines how it's initialized
- objects with _static_ or **thread_storage** duration are initialized _by default_
  - _all_ members initialized to 0
- **objects with automatic or allocated storage must be initialized explicitly**
- Use macros to encapsulate initialization:
  ```c
  #define P99_NEW(T, ...) T ## _init(malloc(sizeof(T)), __VAR_ARGS__)
  ```

### Machine Model

- **von Neumann model**
  - A processing unit has:
    - a finite number of hardware **registers** that hold integer values
    - a **main memory** that holds the program as well as data
      - linearly addressable
    - a finite **instruction set** that describes the operations that can be done with these components
- Two very special registers:
  - `%rbp` - base pointer
  - `%rsp` - stack pointer
- **The Stack**
  - holds local variables and compound literals
  - `%rbp` - upper end of this area

## Text Processing

- `fgetline`
- `strtoul` & `strtoull`
  - _NOT_ `const`-safe
- `memchr` vs `strchr`
  - _NOT_ `const`-safe
- `strcpn` & `strcspn`
  - _are_ `const`-safe

### Formatted Input

- `%lg` - for `double`
- `%Lg` - for `long double`

### Extended Character Sets

- `setlocale()`
  - `setlocale(LC_ALL, "")`
- multibyte characters do _NOT_ contain null bytes
- multibyte strings _are_ null terminated
- `<wchar.h>`
  - `wbstate_t`
  - `wchar_t`
- `mbsrtowcs()` - converts multibyte null-terminated string to wide character representation
  - wide character string _also_ null-terminated, of type `wchar_t`

### Binary Streams

- `fseek` and `ftell` are _NOT_ suitable for very large file offsets
  - up to `LONG_MAX` bytes

## Error Checking and Cleanup

- `<errno.h>`
  - `EOF` - negative
  - `EDOM`
  - `EILSEQ`
  - `ERANGE`

## Performance

- Some potential pitfalls
  - out-of-bounds access of arrays
  - accessing uninitialized objects
  - accessing objects after lifetime has ended
  - integer overflow
- dynamic allocation should be done with `calloc()` instead of `malloc()` wherever suitable
- Functions that receive pointers should use array syntax and distinguish different cases:
  - `void func(double a[static 1]);`
    - pointer to a single object
    - expecting a pointer that is non-null
  - `void func(double a[static 7]);`
    - pointer to a collection of objects of known number
    - expecting a pointer that points to _at least_ that number of elements
  - `void func(size_t n, double a[static n]);`
    - pointer to a collection of objects of _unknown_ number
  - `void func(double* a);`
    - pointer to a single object of the type _or_ a null pointer
- Taking addresses of block-scope (local) variables should be avoided if possible
  - mark all variables in complex code with `register`
  - this helps the optimizer because it inhibits aliasing
- Use `unsigned` integer types for loop indices
  - handle wrap-around explicitly
    - by comparing the loop variable to the maximum value of the type _before_ the increment operation
- Keywords introducing optimizations:
  - `regsiter` - C90
    - can help avoid aliasing between objects defined locally in a function
  - `inline` - C99
  - `restrict` - C99
  - `alignas` (since C23) and `_Alignas` (since C11)
    - related, `alignof`
    - help to position objects on cache boundaries and improve memory access

### Inline functions

- opens up a lot of optimization opportunities
- pointers to the same function in different **Translation Unit**s have different addresses
- the `inline` keyword - does _NOT_ guarantee/force a function to be inlined
- `inline` merely provides a way that it _may_ be
- an unused `inline` function in a specific TU will be _completely_ absent from the binary of that TU
  - will _not_ contribute to its size
- an `inline` definition is visible in _ALL_ TUs
  - any change to an `inline` function triggers a _complete_ rebuild
- an `inline` definition goes in a _header file_
- an _additional_ declaration without `inline` goes in _exactly one TU_
- _ONLY_ expose functions as `inline` if they are considered as _stable_
- local identifiers of the `inline` function (parameters or local variables) may be subject to **macro expansion** for some other macros
  - _ALL_ identifiers local to the `inline` function should be protected by a convenient name convention
  - e.g. `xx_<name>`
- `inline` functions have no particular TU with which they are associated
  - `inline` functions _CANNOT_ access identifiers of `static` functions
  - `inline` functions _CANNOT_ define/access identifiers of _modifiable_ `static` objects
  - the access is restricted to the _identifiers_, _NOT_ the objects/functions themselves
  - you _can_ pass a pointer to a `static` object/function to an `inline` function

### `restrict`

- tells the compiler that the pointer in question is the _only_ access to the object it points to
- compiler assumes that changes to the object can _only_ occur through the same pointer
- the object does _NOT_ alias any other object the compiler handles in this part of code
- `int fputs(const char *restrict s, FILE *restrict stream);`

### Performance Measurement

- **relative standard deviation**
  - standard-deviation(S) / mean(S)
  - only value of a low percentage can be considered good
- **skew**
  - lopsideness (or asymmetry) of the sample
  - perfect symmetry has skew of 0
  - positive value means "tail" to the right

## Function-Like Macros

- e.g. `<tgmath.h>`
- _Whenever possible_, prefer an `inline` function to a functional macro
- In situations where we have a _fixed_ number of arguments with types that are well-modeled by C's type system - we should use functions, _NOT_ macros

### How Function-like Macros Work

- macros are replaced during **preprocessing**, a very early stage of compilation
- Requirements about function-like macros:
  - they evaluate each argument only once
  - parenthesize all arguments with `()`
  - have no hidden effects such as unexpected control flow
  - parameters of macros must be identifiers
- **recursion** is explicitly disabled for macro expansion
- **Function decay**
  - a function `f` without a following opening `(` decays to a pointer to its start
- **Macro retention**
  - if a functional macro is not followed by `()`, it is _NOT_ expanded
- put `()` _around_ macro definition to prevent it from being expanded
- To enforce the use of string literal - `"" S ""`

  - compiler will warn you if you pass a pointer to char

  ```c
  #if NDEBUG
  # define TRACE_PRINTF1 (F, X) do { /* nothing */ } while (false)
  #else
  # define TRACE_PRINTF1 (F, X) fprintf(stderr, "" F "\n", X)
  #endif
  ```

  - but the version above has the possibility of having mismatching args undetected for a long time if `NDEBUG` mode

  ```c
  #if NDEBUG
  # define TRACE_ON 0
  #else
  # define TRACE_ON 1
  #endif

  # define TRACE_PRINTF2 (F, X) \
    do { if (TRACE_ON) fprintf(stderr, "" F "\n", X); } while (false)
  ```

  - _any_ modern compiler should be able to optimize the call to `fprintf` if `TRACE_ON` is `0`
  - but compiler should _not_ omit the argument check for `F` and `X`,
    - because `fprintf` expects it

- Other ways to force a macro argument to be a particular type:
  - prepend `+0` - forces to be any arithmetic type
    - integer,
    - float,
    - pointer
  ```c
  #define TRACE_VALUE0 (HEAD, X) TRACE_PRINT2(HEAD " %Lg", (X)+0.0L)
  ```
  - To print a pointer:
  ```c
  #define TRACE_PTR (HEAD, X) TRACE_PRINT2(HEAD " %p", ((void*){ 0 } = (X)))
  ```
  - `((T){ 0 } = (X))` - check whether `X` is assignment-compatible to type `T`
    - compound literal `(T){ 0 }` creates a _temporary_ object of type `T`, to which we then assign X
    - a modern optimizing compiler should optimize away the use of temp object
    - but a modern optimizing compiler should still do the type checking for us

### Accessing the Calling Context

- `__LINE__` - special macro that _always_ expanded to decimal integer constant for the number of the actual line in the source
  - might _NOT_ fit into an `int`
  - on embedded platforms, `INT_MAX` might be small as `32767`
  - very large sources (automatically produced) may have more lines than that
  - may be avoided by fixing the type to `unsigned long`
  - **using `__LINE__` is inherently dangerous**
- `__DATE__` & `__TIME__`
  - date & time of compilation
- `__FILE__` - name of current **TU**
- `__func__` - a _local_ `static` variable holding name of the current function
- `#` - special operator
  - if a `#` appears _before_ a macro parameter in the expansion, the actual argument to this parameter is _stringified_
    - all its textual content is placed into a string literal
    - stringification with operator `#` does _NOT_ expand macros in its arg
  - _ONLY_ applies to macros arguments
  ```c
  #define TRACE_PRINT5 (F, X)                                     \
  do {                                                            \
    if (TRACE_ON)                                                 \
      fprintf(stderr, "%s:" STRGY(__LINE__) ":(" #X "): " F "\n", \
              __func__, X);                                       \
  } while (false)
  ```
- To fix the potential problems with `__LINE__`
  - convert the line number directly to string
  - the stringification does entirely at compile time
  ```c
  #define STRINFITY(X) #X
  #define STRGY(X) STRINGIFY(X)
  ```
- `##` - **token concatenation operator**

### Variadic Macros

- arguments accessible via `__VA_ARGS__`
- the list in `__VA_ARGS__` _cannot_ be empty/absent
  - wrap `"" __VA_ARGS__ ""`
- format specifier `".0d"` - prints an `int` of width `0`
  - prints _nothing_

```c
/**
Returns the number of arguments in the `...` list
*/
#define ALEN(...) ALEN_0(__VA_ARGS__,             \
 0x1E, 0x1F, 0x1D, 0x1C, 0x1B, 0x1A, 0x19, 0x18,  \
 0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10,  \
 0x0E, 0x0F, 0x0D, 0x0C, 0x0B, 0x0A, 0x09, 0x08,  \
 0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00)

#define ALEN_0(_00, _01, _02, _03, _04, _05, _06, _07,          \
               _08, _09, _0A, _0B, _0C, _0D, _0F, _0E,          \
               _10, _11, _12, _13, _14, _15, _16, _17,          \
               _18, _19, _1A, _1B, _1C, _1D, _1F, _1E, ...) _1E
```

### Variadic Function

- `int printf(char const format[static 1], ...);`
- **Using variadic functions is _NOT_ portable _unless_ each argument is forced to a specific type**
- when passed to a variadic parameter, _all_ arithmetic types are converted as for arithmetic operations, with the exception of `float`
  - when passed to a variadic parameter, `char` and `short` are converted to a wider type, _usually_ `int`
- `NULL` is only guaranteed to be a null pointer constant - compilers are free to choose which variant they provide
  - `void*(0)` - type is `void*`
  - `0` - type is `int`
- **AVOID variadic functions for new interfaces**
  - `<stdarg.h>` if you have to, which defines,
    - `va_list` type
    - 4 function-like macros

```c
double sum_variadic(size_t n, ...) {
  double ret = 0.0;
  va_list va;
  va_start(va, n); // maintain the length of the variadic argument list
  for (size_t i = 0; i < n; ++i) {
    ret += va_arg(va, double);
  }
  va_end(va);
  return ret;
}
```

### Type-Generic Programming

- addition of C11
- C99 has `<tgmath.h>` for type-generic math functions
  - `_Generic`
  ```c
  _Generic(<controlling-expression>,
    <type1>: <expression1>,
    ...,
    <typeN>: <expressionN>,
  )
  ```
- simplest use case for `_Generic`: for a type-generic macro interface by providing a choice between function pointers
  - e.g. the `fabs` macro in `<tgmath.h>`
  ```c
  #define fabs(X)       \
  _Generic((X),         \
    float: fabsf,       \
    long double: fabsl, \
    default: fabs       \
  )(X)
  ```
  - for multiple arguments, the type needs to be decided on a _combination_ of those types
    - using the _sum_ of the arguments as a **controlling expression**
    - argument promotions & conversions are effected to the arguments of that plus operation
    - the wider of the types
  ```c
  #define min(A, B)     \
  _Generic((A) + (B),   \
    float: minf,        \
    long double: minl,  \
    default: min        \
  )((A), (B))
  ```
- result type of a `_Generic` expression is the type of the chosen expression
- using `_Generic` with `inline` functions adds optimization opportunities
- Type of the `<controlling-expression>` - _as if_ it were passed to a function
  - if any, **type qualifiers** are _dropped_ from the type of of the controlling expression
  - array type is converted to a pointer type to the base type
  - function type is converted to a pointer to a function
- **the type expressions in a `_Generic` expression should only be _unqualified types_: no array type, and no function types**

```c
/**
The max value for the type of argument X
*/
#define MAX_VAL(X) \
_Generic((X),  \
         bool: (bool)+1, \
         char: (char)+CHAR_MAX, \
         singed char: (signed char)+SCHAR_MAX, \
         unsigned char: (unsigned char)+UCHAR_MAX, \
         signed short: (signed short)+SHRT_MAX, \
         unsigned short: (unsigned short)+USHRT_MAX, \
         signed: INT_MAX, \
         unsigned: UINT_MAX, \
         signed long: LONG_MAX, \
         unsigned long: ULONG_MAX, \
         signed long long: LLONG_MAX, \
         unsigned long long: ULLONG_MAX, \
         float: FLT_MAX, \
         double: DBL_MAX, \
         long double: LDBL_MAX, \
)
```

- To determine the type of _promoted_ types - when fed to an arithmetic operation
  - `0+(XT)+0`
  - valid if `XT` is a variable _or_ a type
  - if it's a variable
    - integer promotion
  - if it's a type
    - `(XT)+0` - cast of `+0` to type `XT`
- in a `_Generic` expression, the type expression choices must be unambiguous at compile time
  - must refer to mutually incompatible types
  - _CANNOT_ be a pointer to a VLA
- **ALL choice `<expression1>` ... `<expressionN>` in a `_Generic` must be valid**
  - `((void*){ 0 } = (1LL))` is _NOT_ valid
    - assigning a non-zero integer to a pointer

## Control Flow

- code composed of basic blocks stitched together by `if`/`else` or loop statements have better optimization opportunities for the compiler
- `_Noreturn`
  - `exit`
  - `abort`
- _Long_ jumps
  - `setjmp`
  - `longjmp` - can jump across function boundaries
  - `getcontext`
  - `setcontext`
- **interrupts** - `signal`s
- **threads**
  - `thrd_create`
  - `thrd_exit` - terminates the execution of the calling thread
- Difficulties with unexpected control flow:
  - objects could be used outside their lifetime
  - objects could be used uninitialized
  - values of objects could be misinterpreted by optimizing
    - mitigated by `volatile`
  - objects could be partially modified
    - mitigated by `sig_atomic_t`, `atomic_flag`, or `_Atomic`
    - with the lock-free property and **relaxed** consistency
  - updates to objects could be sequenced unexpectedly
    - all `_Atomic`
  - execution ust be guaranteed to be exclusive inside a _critical section_
    - `mtx_t`

### Sequencing

- evaluation of function arguments can occur in _any_ order
- **indeterminately sequenced**:
  - can result from **side effects**
  ```c
  unsigned add(unsigned* x, unsigned const* y) {
    return *x += *y;
  }
  int main(void) {
    unsigned a = 3;
    unsigned b = 5;
    printf("a = %u, b = %u\n", add(&a, &b), add(&b, &a));
  }
  ```

### Short Jumps

- `goto` - jump to marked positions _inside_ the _same_ function
- each iteration defines a new instance of a local object
- `goto` should _ONLY_ be used for exceptional changes in control flow
  - ie. when encountering a transitional error condition that requires local cleanup

### Function Calls

- Each function call defines a new instance of a local object

### Long Jumps

- `longjmp` _never_ returns to the caller
  - has `_Noreturn`
- `jmp_buf` - defined in `<setjmp.h>`
  - an **opaque** type
  - _NEVER_ make assumptions about its structure or individual fields
  - `typedef` for `jmp_buf` hides an array type
  - an object of type `jmp_buf` _CANNOT_ be assigned to
  - `jmp_buf` function parameter is rewritten to a pointer to `jmp_buf_base`
    - thus modifiable
    - such function _always_ refers to the original object, _NOT_ to a copy
- under normal execution flow, `setjmp` returns 0 - meaning no special jumps occurred
- if a `longjmp` occurs, it jumps to the `jmpTarget` defined by `setjmp(jmp_buf jmpTarget)`
  - then `setjmp` returns _non-zero_ value, depending the second argument of `longjmp`,
    - `_Noreturn void longjmp(jmp_buf jmpTarget, int condition);`
- Leaving the scope of a call to `setjmp` _invalidates_ the jump target
- after call to `longjmp`, a `0` as a `condition` parameter to `longjmp` is replaced by `1`
- `setjmp`/`longjmp` is efficient
  - usually no more than 10 to 20 assembler instructions
- Normal/usual strategies by lib implementations:
  - `setjmp` saves essential hardware registers, including stack & instruction pointers, in `jmp_buf` object
  - `longjmp` restores them from there and passes control back to the stored **instruction pointer**
- `setjmp` may be used _only_ in simple comparisons inside controlling expressions of conditionals
  - `switch`
  - `==`, `<`, etc
  - return value of `setjmp` may _NOT_ be used in a assignment
- Objects modified across `longjmp` must be `volatile`
  - optimizations interacts badly with calls to `setjmp`
- `volatile`
  - `volatile` objects are reloaded from memory _each time_ they are accessed
  - `volatile` objects are stored to memory _each time_ they are modified

### Signal Handlers

- Two kinds of external events that interrupt
  - **hardware interrupts**, a.k.a. **traps** or **synchronous signals**
  - **software interrupts**, a.k.a. **asynchronous signals**
- Most modern processors have built-in feature to handle hardware interrupts
  - **interrupt vector table**
  - indexed by different hardware faults known to the platform
  - entries are pointers to procedures (**interrupt handlers**)
- `<signal.h>`
  - hardware:
    - `SIGFPE`
    - `SIGILL`
    - `SIGSEGV`
  - software:
    - `SIGABRT`
    - `SIGINT`
    - `SIGTERM`
- Two standard dispositions for handling signals:
  - `SIG_DFL` - restores platform's default handler for the specific signal
  - `SIG_IGN` - signal to be ignored
- `void (*signal(int sig, void(*handler)(int))) (int);`
  - sets the handler for signal `sig`
  - provided in `<signal.h>`
- `int raise(int);` - in `<signal.h>`
  - deliver the specified signal to the current execution
- `raise`/`signal`
  - current state of execution is memorized
  - control flow is passed to the signal handler
  - a return from there restores the original execution environment and continues execution
- `SIGABRT`
  - calls `_Exit`
  - does _NOT_ call any cleanup callers
- `SIGTERM`
  - calls `quick_exit`
  - goes through list of cleanup handlers registered with `at_quick_exit`
- `SITINT`
  - chooses the `default` case of the signal handler
- after return from a signal handler, execution resumes _exactly_ where it was interrupted
- `SIGCONT` - _not_ in C standard?
  - but POSIX
- a signal handler is _NOT_ supposed to change the state of execution
- objects that are potentially changed by a signal handler must be `volatile` qualified
  - compiler has no idea when interrupt handlers may kick in
- A C statement may correspond to several processor instructions
  - e.g. `double x` could be stored in two machine words
    - an assignment (write) of `x` into memory could need two separate assembler statements to write both halves
  - if such assignment is split in half by a signal
  - **signal handlers need uninterruptible operations**
- **Uninterruptible operations**
  - always _indivisible_ in the context of signal handlers
- Classes of types in C that provide uninterruptible operations:
  1. type `sig_atomic_t` - integer type with min width of 8 bits
  - should _NOT_ be used as counters
  - e.g. `++` could be split to three operations (load, increment, store) and easy to overflow
  2. type `atomic_flag`
  3. all other atomic types that have lock-free property
  - both 2. and 3. are in C11
  - only present if
    - test macro `__STDC_NO_ATOMICS__` has _NOT_ been defined by the platform, _and_
    - header `<stdatomic.h>` included
- Signal handlers for async signals can _only_ call functions that are **asynchronous signal safe**
  - C standard defines a few:
    - `_Noreturn` functions
      - `abort`
      - `_Exit`
      - `quick_exit`
      - _NOT_ `exit`!!!
    - `signal()` for the same signal number
    - _some_ functions acting on atomic objects
- **Unless otherwise specified, C lib functions are _NOT_ asynchronous signal safe**
- Object specified with `_Atomic` can be used with the same operators as other objects with the same base type
  - such objects guaranteed to avoid race conditions with other threads
  - _uninterruptible_ if type has the **lock-free property**, which tested by the feature-test macro `ATOMIC_LONG_LOCK_FREE`
    - `#if ATOMIC_LONG_LOCK_FREE > 1`

## Threads

- `<threads.h>`
- Two principal function interfaces

  - `thrd_create`

    ```c
    #include <threads.h>
    typedef int (*thrd_start_t)(void*);
    int thrd_create(thrd_t*, thrd_start_t, void*);
    int thrd_join(thrd_t, int*);
    ```

  - `thrd_join` - allows one thread to wait until another is finished

- `thrd_start_t` - pointer to a function
  - receives `void*` and returns `int`
  - function is executed at the start of the new thread
- `thrd_t` - _opaque_ type, identifying the new thread
- To prevent **race conditions**
  - `_Atomic`
- **atomic specifier** vs. **atomic qualifier**
  ```c
  // `A`: atomic pointer to an array of 45 doubles
  extern _Atomic(double (*)[45]) A;
  extern double (*_Atomic A)[45];
  ```
  ```c
  // `B`: pointer to array of 45 atomic doubles
  extern _Atomic(double) (*B)[45];
  extern double _Atomic (*B)[45];
  ```
  - **Use the specifier syntax `_Atomic(T)` for atomic declarations**
- `_Atomic` _CANNOT_ be applied to array types
  - _NO_ atomic array types
- **Atomic objects are the privileged tools to force the absence of race conditions**

### Race-Free Initialization & Destruction

- `call_once`
  - `void call_once(once_flag* flag, void cb(void));`
  - registers a callback function `cb` that should be called at _exactly one point_ of the execution
  - `cb` is registered with a _specific_ object, with type `once_flag`
- `once_flag` - an **opaque** type, which guarantees enough state to
  - determine whether a specific call to `call_once` is the very first among threads
  - _ONLY_ call the `cb` then
  - _NEVER_ call the `cb` again
  - hold back all other threads until the one-and-only call to `cb` has finished
- _ALL_ stream functions (except `fopen` and `fclose`) are race-free
- Concurrent write operations should print entire lines at once
- To ensure a safe and race-free destruction and deallocation of shared dynamic objects,
  - the easiest way is to wait until we are sure there are no threads around, i.e. when exiting the entire program execution
  ```c
  static void errlog_fclose(void);
  // register with `atexit` as soon as the _initializing_ function `errlog_fopen` is entered
  void errlog_fopen(void) {
    atexit(errlog_fclose);
  }
  ```

### Thread-local Data

- Pass thread-specific data through function arguments
- Keep thread-specific state in local variables
- If the above ^^^ are not possible or too complicated, consider using a special storage class and a dedicated data type
  - `_Thread_local` - storage class specifier, forcing a thread-specific copy of the variable
  - macro `thread_local`, in `<threads.h>`, which expands to the keyword
  - a `thread_local` variable has a separate instance for each thread
- `thread_local` variables must be declared in file scope, or `static`
  - _CANNOT_ be initialized dynamically
- **Use `thread_local` if initialization can be determined at compile-time**
- `tss_t` - thread-specific storage, if a storage class specifier is not sufficient
  - maybe dynamic initialization or destruction
  - `key` - an **opaque** ID

```c
void* tss_get(tss_t key); // returns pointer to object
int tss_set(tss_key, void* val); // return an error indication

/**
The function that is called at the end of a thread to destroy thread-specific data,
is specified as a function pointer of type `tss_dtor_t`, when the `key` is created
*/
typedef void (*tss_dtor_t)(void*); // pointer to destructor
int tss_create(tss_t key, tss_dtor_t dtor); // returns error indication
void tss_delete(tss_t key);
```

### Critical Data & Critical Sections

- `mtx_t` - a **mutext** type
  - **mutual exclusion**
  - in `<threads.h>`
- `mtx_lock` & `mtx_unlock`
  - call `mtx_lock` to block execution of the calling thread until it can be guaranteed that no other thread is inside the critical section protected by the same mutex
  - `mtx_lock` _acquires_ the lock on the mutex and _holds_ it
  - `mtx_unlock` _releases_ the lock
- Mutex operations guarantee linearizability
- `int mtx_trylock(mtx_t*);`
  - tests whether other threads already holds the lock
- `mtx_timedlock(mtx_t*);` - avoids locking forever
  - only allowed if mutex initialize with type `mtx_timed`
- `int mtx_init(mtx_t* mtx, int);`
  - _Every_ mutex must be initialized with `mtx_init`
  - second parameter (the `int`) - type of the mutex
    - `mtx_plain`
    - `mtx_timed`
    - `mtx_plain|mtx_recursive`
    - `mtx_timed|mtx_recursive`
- `mtx_recursive` - allows `mtx_lock` to be called several times successively for the same thread, without unlocking it beforehand
  - mostly used for recursive functions that call `mtx_lock` on critical section and call `mtx_unlock` on exit
- a mutex must be released before the termination of the thread
- a mutex must be _destroyed_ at the end of its lifetime
  - `mtx_destroy()`
- `mtx_destroy()` must be called
  - before the scope of a mutex with automatic storage duration ends
  - **and before memory of a dynamically allocated mutex is freed**

### Communication through condition variables

- `cnd_timedwait`
  - `int cn_timedwait(cnd_t* restrict cond, mtx_t* restrict mutex, const struct timespec* restrict time_point);`
  - automatically unlocks the mutex (so other threads can do their job to assert the condition expression),
  - and blocks on the condition variable pointed to by `cond`, until:
    - thread is signalled by `cond_signal` or `cnd_broadcast`, or
    - until `TIME_UTC` based time point has been reached, or
    - by spurious wakeup occurs
  - then mutex is locked again _before_ the function returns
- `cnd_t` - type of **conditional variable**
  - to identify a condition for which a thread might want to wait
  - a `cnd_t` must be:
    - initialized dynamically
    - destroyed at the end of its lifetime
- `cnd_signal` & `cnd_broadcast`
  - `cnd_broadcast` wakes up all threads waiting for the same condition variable
  - _BUT_ the threads are _NOT_ waken up all at once
  - they are waken up one after another as they reacquire the mutex
- A condition variable can only be used _simultaneously_ with one mutex
  - **best practice to never change the mutex used with a condition variable**
- But there can be many condition variables for the same mutex

### More Sophisticated Thread Management

- returning from `main` or calling `exit` _terminates all_ threads
- To avoid terminating other threads when terminating `main`:
  - use `thrd_exit(0);` to terminate `main` (instead of `return`)
  - install an `atexit()` handler to arrange for necessary cleanup
  - the thread that returns last executes the `atexit` handlers
- Good coding style to tell the system that a thread will never be **join**ed
  - we **detach** the corresponding thread, by
  - inserting `thrd_detach` at beginning of thread function
  ```c
  // nobody should ever wait for this thread
  thrd_detach(thrd_current());
  // delegates part of the job to an auxiliary thread
  thrd_create(&(thrd_t){0}, account_thread, Lv);
  ```
- `thrd_detach()`
  - resources held by the thread will be freed automatically once thread exits
- while blocking on `mtx_t`/`cnd_t`, a thread frees processing resources
- `thrd_sleep`
- `thrd_yield` - terminates the current time slice and waits for the next processing opportunity

## Atomic Access and Memory Consistency

### **Happened Before**

- Between threads, the ordering of events is provided by **synchronization**
- Two types of **synchronization**:
  - ops on atomics
  - certain C lib calls
- The set of modifications of an atomic object X are performed in an order that is consistent with the sequenced-before relation of any thread that deals with X
  - **modification order of X**
  -

### C lib Calls that Provide Synchronization

- `cnd_signal` & `cnd_broadcast` _synchronize_ via the mutex
- **Calls to `cnd_signal` & `cnd_broadcast` should occur inside a critical section that is protected by the same mutex as the waiters**

### Sequential Consistency

- `void atomic_init(A volatile* obj, C des);`
  - does _NOT_ imply synchronization!
  - concurrent calls from different threads _can_ produce a race!
  - a cheap form of assignment

### Other Consistency Models

- Synchronizing functional interfaces for atomic objects have a form with `_explicit` appended
  - allows us to specify their **consistency model**

## Misc

- `strstr` finds pattern in a string then returns pointer
- if no return type specified for a function, `int` is assumed
- command `cc` to compile source file
- `sqrt`, `sin`, `cos`, and `cos` return `double`
- external linkage
- `atof` from `<stdlib.h>`
- `push(pop() - pop())` is WRONG!
  - We cannot guarantee the order
- `ungetc()` in `<stdio.h>`
- Scoping:
  - scope of an external variable or a function lasts from _the point at which it
    is declared to the end of the file being compiled_
  - use `extern` if an external variable is to be referred to before it is
    defined, or if it is defined in a different file from the one where it's
    being used
  - distinguish between `declare` and `define`
  - `extern double val[]` => declare the variable, but does not reserve storage
  - only one _definition_ is allowed for an external variable among all files that
    make up the program
    - other files my only contain `extern` declarations to access it
    - array sizes must be specified with the definition, but are optional with
      `extern` declaration
    - initialization of an external variable only with definition
- Up to some moderate program size, it's probably best to have one header file
  that contains everything that's to be shared between any two parts of the
  program
- the `static` declaration, applied to external variables or functions, limits the
  scope of that object to the rest of the source file being compiled
- `static` can also apply to function names
  - normally, function names are global, visible to the entire program
  - `static` functions, however, are invisible outside of the source file where
    declared
- `static` _internal_ variables are private and have permanent storage within the
  function
- `register`
  - the variable in question is heavily used
  - will be placed in machine registers - faster
  - compilers are free to ignore this advice
  - _ONLY_ applies to automatic variables and to formal parameters of a function
  - excess `register` declarations are harmless and will be ignored/disallowed
  - _NOT_ possible to take the address of a `register` variable
- C is _NOT_ a block-structured lang, since functions may not be defined within
  other functions
- `{` is _brace_
- an automatic variable declared and initialized in a block is initilaized each
  time the block is entered
  - a `static` variable is initialized _ONLY_ the first time the block is entered
- _Initialization_
  - if no explicit initialization:
    - external and static variables are guaranteed to be _zero_
    - automatic and register variables have _undefined/garbage_ values
  - for _external_ and _static_ variables, the initializer must be a constant
    expression
  - for _automatic_ and _register_ variables, the initializer may be any expression
  - for an array, if fewer initializers than the number specified, the missing
    elements will be _zero_ for _external_, _static_, and _automatic_ variables
  - there's no way to specify repetition of an initializer, nor to initialize an
    element in the middle of an array without supplying all preceding values
  - `char pattern[] = "ould"` is equal to
    `char pattern[] = {'o', 'u', 'l', 'd', '\0'}` (size is _five_)
- `strlen` does NOT include the ending `null` value
- Recursion
  - each invocation gets a fresh set of all automatic variables,
  - independent of the previous set
- Preprocessors
  - a separate first step in compilation
  - Macro Substitution
    - `#define name replacement_text`
    - in `#define`, a long definition may be continued onto several lines by
      placing a `\` at the end of each line to be continued
    - `#undef` undefines names
    - formal parameters are _NOT_ replaced within quoted strings
    - `#define dprint(expr) printf(#expr " = %g\n", expr)`
    - `##` concatenates actual arguments
      - `#define paste(front, back) front ## back`
  - Conditional Inclusion
    - `#if !defined(HDR) #define HDR #endif`
- generic pointer: `void * (pointer to void)` -> it holds any type of pointer, but
  _cannot_ be deferenced
- the `&` operator _ONLY_ applies to objects in _memory_ - variables and array
  elements
- `&` does _NOT_ apply to expressions, constants, or `register` variables
- unary operators like `*` and `++` associate right to left
- _IMPORTANT_: C passes arguments to functions _by value_
  - there's no direct way for the called function to alter a variable in the
    calling function
- the value of a variable/expression of type array is the address of element
  zero of the array
  - `arr_ptr = &arr[0]` === `arr_ptr = arr`
  - `&arr[i]` === `arr + i`
  - `arr[i]` === `*(arr + i)`
  - `arr_ptr[i]` === `*(arr_ptr + i)`
- when an array name is passed to a function, what is passed is the _location_ of
  the initial element
  - but within the called function, this argument is a local variable
- `char str[]` === `char* s`
  - but we prefer the latter
- `func(&arr[2])` === `func(arr + 2)` - pass to the function `func` the address of the
  subarray starting at `arr[2]`
- C language gurantees that `0` is _NEVER_ a valid address for data
- number `0` is interchangeable with pointers
- `NULL`
- behavior is undefined for arithmetic or comparisons with pointers that do not
  point to members of the same array
- `<stddef.h>` defines a type large enough for pointer differences: `ptrdiff_t`
- C does _NOT_ provide any operators for processing an entire string of characters
  as a unit
- prefix & postfix:
  - `*p++ = val; /* push val onto stack */`
  - `val = *--p; /* pop top of stack into val */`
- if a two-dimensional array is to be passed to a function,
  - parameter declaration must include number of columns
  - number of rows is _irrelevant_
  - `func(int (*day_dictionary)[12])`
- array of pointers: `int *ptr[num]`
  - the definition only allocates `num` pointers and does not initialize them
  - initialization must be done explicitly
- `argc` is at least `1`
- `int (*cmp)(void *, void *)` - a pointer to a function that has two `void *`
  arguments and returns an `int`
- declarations cannot be read left to right
- `*` _is a prefix operator and it has lower precedence than_ `()`
  - `int *f()` - a function returning a pointer to `int`
  - `int (*pf)()` - a pointer pointing to a function that returns an `int`
- `char (*(*x())[])()`
  - from inner to outer?
  - a function returning a pointer pointing to an array of pointers pointing to
    a function that returns `char`
- `char (*(*x[3])())[5]`
  - an array of three pointers pointing to a function that returns pointer
    pointing to array of five `char`s
- `isalpha` & `isalnum`
- _dcl_ and _direct-dcl_
- `strcat` - `<string.h>`
- structures can be copied and assigned to, passed to functions, and returned by
  functions
- `structure tag` - optional
- `members`: variables named in a sturcture
- `struct { ... } x, y, z;` - space allocated for them
- a `struct` declaration _NOT_ followed by a list of variables reserves _NO_ storage
- `struct point { ... }` - `struct point` is the data type
  - `struct point one_point;`
  - a variable `one_point` which is of type `struct point`
  - to initialize: `struct point another_point = {123, 456};`
- structures can be nested
- the only legal operations on a structure:
  - copy and assign to it as a unit
  - take its address with `&`
  - access its members
- structures may _NOT_ be compared
- an automatic structure may also be initialized by an assignment
- structure is passed to a function by value, like any others
- generally more efficient to pass a pointer than to copy the whole structure
- the structure member operator `.` is higher than `*`
- if `struct pt *pp`, then
  - `(*pp).member_one` === `pp->member_one`
  - both `.` and `->` associate from left to right
- you _CANNOT_ add two pointers
- the language definition does gurantee that pointer arithmetic that involves
  the first element beyond the end of an array (`&ptr[length_of_array]`) will work
  correctly
- `malloc` ensures alignment
- _hash search_
- `typedef`
  - `typedef int (*PFI)(char*, char*);`
  - `PFI` is a pointer to function (of two `char*` arguments) returning `int`
  - `PFI strcmp, numcmp;`
- `union`

  - it's a single variable that can legitimately hold any _one_ of several types
  - the compiler is responsible for keeping track of size and alignment
    requirements
  - use some variables like `utype` to keep track of the current type stored in
    `union`

  ```
  struct {
      char* name;
      int flags;
      int utype;
      union {
          int ival;
          float fval;
          char* sval;
      } u;
  } symtab[NSYM];

  symtab[i].u.ival;
  *symtab[i].u.sval;
  symtab[i].u.sval[0];
  ```

  - a `union` may _ONLY_ be initialized with a value of the type of its first member

- Bit-fields

  - it may be necessary to pack several objects into a single machine word
  - `enum { KEYWORD = 01, EXTERNAL = 02, STATIC = 04 };`
  - the numbers must be powers of two
  - `flags |= EXTERNAL | STATIC` - turn on the `EXTERNAL` and `STATIC` bits in `flags`
  - `flags &= ~(EXTERNAL | STATIC)` - turn them off
  - `if ((flags & (EXTERNAL | STATIC)) == 0)` is true if both bits are _off_
  - a _bit field_, or _field_ for short, is a set of adjacent bits within a single
    implementation-defined storage unit that we will call a "word"

  ```
  /* defines a variable called flags that contains three 1-bit fields */
  struct {
      unsigned int is_keyword : 1;
      unsigned int is_extern : 1;
      unsigned int is_static : 1;
  } flags;

  flags.is_extern = flags.is_static = 1; /* turn the bits on */
  flags.is_extern = flags.is_static = 0; /* turn them off */
  ```

  - fields may be declared _only_ as `int`s; for protability, specify `signed` or
    `unsigned` explicitly
  - fields are not arrays and do not have addresses - _cannot_ apply `&` to them

- the I/O library might convert carriage return and linefeed to newline on input
  and back again on output
- `EOF` defined in `<stdio.h>`; normally its value is `-1`; use `EOF` wherever you can
- `prog <infile` - file `prog` to read characters from `infile` instead
- `otherprog | prog` - run both programs and _pipe_ the standard output of `otherprog`
  into the standard input of `prog`
- `putchar` returns the character written, or `EOF` if an error occurs
- `prog >outfile` - write the standard output to `outfile` instead
- `prog | anotherprog` - _pipe_ the standard output of `prog` into the standard input
  of `anotherprog`
- `tolower` and `toupper` in `<ctype.h>`
- `printf` returns the number of characters printed
- `printf("%.*s", max, str)` - print at most `max` characters from a string `str`
- `sprintf` stores the output in a string, instead of into stdout
- the proper declaration for `printf` is:
  - `int printf(char* fmt, ...);`
- `<stdarg.h>` contains macros that define how to step through an argument list
- `va_list` declares a variable that refers to reach argument in turn
- the macro `va_start` initializes `va_list` arg pointer to point to the first unnamed argument
- `va_arg` returns one argument and steps `arg_ptr` to the next
- `va_start` must be called once, before `va_list arg_ptr` is used
  - there must be _at least one_ named argument
  - the final named argument is used by `va_start` to get started
  - `va_end` must be called before the function returns
- `scanf()` and `sscanf()` - basically all their arguments should be pointers
- _formfeed_: `\f`
- `scanf` _WILL_ read across line boundaries to find its input, since newlines are
  just while space
- in a format string, `h` means an `int` to be printed as `short`, `l` means `long`
- `fopen` returns a `file pointer`, which points to a structure that contains
  information about the file
  - location of a buffer
  - current character position in the buffer
  - whether the file is being read or written
  - whether errors or end of file have occurred
- `FILE` - a structure declaration included in `<stdio.h>`
  - `FILE *fp;`
  - `FILE *fopen(char* name, char* mode);`
  - `int fclose(FILE* fp);`
- if there's an error, `fopen` will return `NULL`
- `int getc(FILE* fp);` - returns next character from the stream referred to by
  `*fp`
- `int putc(int c, FILE* fp)`
- When a C program is started, OS opens three files and provides file pointers
  for them; the three files are:
  - standard input - `stdin`
  - standard output - `stdout`
  - standard error - `stderr`
- `int fscanf(FILE* fp, char* format, ...);`
- `int fprintf(FILE* fp, char* format, ...);`
- `freopen()`
- `stdrrr` normally appears on screen _even if_ the standard output is redirected
- `exit`
  - a standard library function
  - terminates program execution
  - it calls `fclose` for each open output file, to flush out any buffered output
  - with `main`, `return expression;` is equivalent to `exit(expression);`
  - `exit()` has the advantage that it can be called from other functions
- `ferror` returns _non-zero_ if an error occurred on stream `fp`
  - `int ferror(FILE* fp);`
- a production program should check for output errors as well
- `feof(FILE* fp)` is analogous to `ferror`
  - `int feof(FILE* fp);`
- `fgets`
  - reads the next input line (including newline) from file `fp` into `line`
  - `char* fgets(char* line, int maxline, FILE* fp);`
  - at most `maxline - 1` characters will be read
  - resulting `line` terminated with `'\0'`
  - normally returns `line`; if error or end of file, returns `NULL`
- `fputs`
  - writes a string (not necessarily with a newline) to a file
  - `int fputs(char* line, FILE* fp);`
- `gets` and `puts` - on `stdin` and `stdout`
  - `gets` deletes the terminal `\n` and `puts` adds it
- `strchr` and `strrchr`
  - `strchr(s, c)` - return pointer to first `c` in `s` or `NULL`
  - `strrchr(s, c)` - return pointer to last `c` in `s` or `NULL`
- `ungetc` - standard library
  - `int ungetc(int c, FILE* fp);`
  - pushes character `c` back onto file `fp`, and returns `c` or `EOF` if error
  - _ONLY ONE_ character of pushback is guaranteed per file
- `system(char* s);`
- Storage management:
  - `void* malloc(size_t n);`
  - `void* calloc(size_t n, size_t size);`
  - `calloc` returns a pointer to enough space for an array of `n` objects of the
    specified `size` or `NULL`; storage is initialized to _zero_
  - `free(ptr)` frees space pointed to by `ptr`, obtained by `malloc` or `calloc`
  - _ERROR_ if `free` something _not_ obtained by `calloc` or `malloc`
  - _ERROR_ to use something after it's been `free`d
- `RAND_MAX` - defined in `<stdlib.h>`
- in UNIX, all input and output is done by reading/writing files
  - all peripheral devices, even keyboard and screen, are files
  - a single homogeneous iterface handles all communication
  - `file descriptor` - just an `int`
- when the command interpreter (_shell_) runs a program, three files are open:
  - `stdin` - _file descriptor_ `0`
  - `stdout` - _file descriptor_ `1`
  - `stderr` - _file descriptor_ `2`
- normally _file descriptor_ `2` remains attached to screen
- Low level I/O - Read & Write
  - `int read(int file_descriptor, char* buf, int n);`
    - a return value of `0` implies `EOF`
    - `-1` means error
  - `int write(int file_descriptor, char* buf, int n);`
  - any number of bytes can be read/written in one call
  - _one_ byte read/written at one call is the most common way
    - _unbuffered_
  - also `1024` or `4096` to a physical block size on a peripheral device
  - larger sizes will be more efficient because fewer system calls
  - constant `BUFSIZ` is defined in `<stdio.h>` (I think...)
  - `read` and `write` are defined in `<unistd.h>`
- `open` vs. `fopen`
  - `open` returns a `file descriptor`, instead of file pointer
  - `open` returns `-1` if error
  - `int open(char* name, int flags, int perms);`
    - `flags`:
      - `O_RDONLY`
      - `O_WRONLY`
      - `O_RDWR`
    - we can treat `perms` as it's always `0`
      - _permission_?
- `create`
  - `fd = create(name, perms);`
  - it will truncate it to zero if the file already exists
  - it's not an error to `create` a file that already exists
  - defined in `<fcntl.h>`
- `vprintf`, `vfprintf`, & `vsprintf`
- `close(int fd);`
- `unlink(char* name)` - removes the file `name` from file system
  - corresponds to `remove` from the standard library
- input and output are normally sequential
  - each `read` or `write` takes place at a position in the file right after the
    previous one
- `lseek`
  - move around in a file without reading/writing any data
  - `long lseek(int fd, long offset, int origin);`
  - `origin` can be `0`, `1`, or `2`
    - `offset` to be measured from the beginning, current position, or th end
  - treat files like large arrays
  - but _slower_ access
  - return value:
    - `long` number: new position in file
    - `-1` if error occurs
  - `fseek`
- files in the standard library are described by file pointers not descriptors
- `#define OPEN_MAX 20 /* max #files open at once */`
- `stderr` to be written unbuffered
- `int _fillbuff(FILE*);`
- the space `malloc` manages may not be contiguous
  - its free space is kept as a list of free blocks
- _first fit_ vs. _best fit_
  - scan the free list until a big-enough block is found
  - ...or look for the smallest block that stisfies the request
- `free`ing also searches through the free list, looking for the proper place to
  insert the block being freed
- for `malloc`, there's a _most restrictive type_:
  - `double`
  - or `long`
- each free block has a _header_ that contains the control information
  - the block allocated constains one more unit - the header itself
  - but the pointer returned by `malloc` points at the free space, _NOT_ at the
    header itself
- `sbrk(n)` returns a pointer to `n` or more bytes of storage; `-1` if no space

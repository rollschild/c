#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef MAX_NUM_LINES
#define MAX_NUM_LINES 4096
#endif

#ifndef MAX_LEN
#define MAX_LEN 256
#endif

int read_lines(char *[], int);  // limit
void write_lines(char *[], int); // num of lines to write
void quick_sort(char *[], int, int, int (*)(char *, char *));
void swap(char *[], int, int); // left and position
int get_line(char *, int);     // limit
int numcmp(char *, char *);

int main(int argc, char **argv) {
    int num_of_lines = 0;
    char *lines[MAX_NUM_LINES];
    bool numeric = (argc > 1 && strcmp(argv[1], "-n") == 0) ? true : false;

    if ((num_of_lines = read_lines(lines, MAX_NUM_LINES)) > 0) {
        quick_sort(lines, 0, num_of_lines - 1,
                   (int (*)(char *, char *))(numeric ? numcmp : strcmp));
        write_lines(lines, num_of_lines);
        return 0;
    } else {
        printf("Error: too many lines!\n");
        return -1;
    }
}

void quick_sort(char *lines[], int left, int right,
                int (*comp)(char *, char *)) {
    if (left >= right)
        return;
    int pos = 0;
    int pivot = (left + right) / 2;
    swap(lines, left, pivot);
    pivot = left;
    for (pos = pivot + 1; pos <= right; ++pos) {
        if ((*comp)(lines[pos], lines[left]) < 0) {
            swap(lines, ++pivot, pos);
        }
    }
    swap(lines, left, pivot);
    quick_sort(lines, left, pivot - 1, comp);
    quick_sort(lines, pivot + 1, right, comp);
}

void swap(char *lines[], int left, int right) {
    char *tmp = lines[left];
    lines[left] = lines[right];
    lines[right] = tmp;
}

int read_lines(char *lines[], int limit) {
    int found = 0;
    char line[MAX_LEN];
    char *ptr;
    int len;
    while ((len = get_line(line, MAX_LEN)) > 0) {
        if (found >= limit ||
            (ptr = (char *)malloc(sizeof(char) * len)) == NULL) {
            printf("Error reading a line!\n");
            return -1;
        } else {
            line[len - 1] = '\0';
            strncpy(ptr, line, len);
            lines[found++] = ptr;
        }
    }

    return found;
}

void write_lines(char *lines[], int num_of_lines) {
    for (int i = 0; i < num_of_lines; ++i) {
        printf("%s\n", lines[i]);
    }
    return;
}

int get_line(char line[], int limit) {
    int c;
    int pos = 0;
    while (pos < limit - 1 && (c = getchar()) != EOF && c != '\n') {
        line[pos++] = c;
    }
    if (c == '\n') {
        line[pos++] = c;
    }
    line[pos] = '\0';
    return pos;
}

int numcmp(char *str1, char *str2) {
    double num1 = atof(str1);
    double num2 = atof(str2);
    return num1 < num2 ? -1 : (num1 == num2 ? 0 : 1);
}


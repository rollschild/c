#include <stdio.h>

int
main() {
    struct test_struct {
        int num;
        char* str;
    };

    struct test_struct struct_1 = {2037, "hello"};
    struct test_struct struct_2 = {1026, "hey"};
    struct test_struct* ptr_1 = &struct_1;
    struct test_struct* ptr_2 = &struct_2;

    printf("is %p smaller than %p? %s\n", ptr_1, ptr_2,
           (ptr_1 < ptr_2) ? "Yes" : "No");
    // You *CAN* compare these two pointers

    return 0;
}

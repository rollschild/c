#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static size_t
numberline_inner(char const* restrict act, size_t numb[restrict], int base) {
    size_t n = 0;
    for (char* next = 0; act[0]; act = next) {
        numb[n] = strtoull(act, &next, base);
        if (act == next)
            break;
        ++n;
    }
    return n;
}

size_t*
numberline(size_t size, char const lbuf[restrict size], size_t* restrict np,
           int base) {
    size_t* ret = 0;
    size_t n = 0;

    /**
     * check for validity of the string first, by
     * finding the first occurrence of (unsigned char)ch,
     * returning pointer to location;
     * This is to check whether it's a `0`-terminated string
     */
    if (memchr(lbuf, 0, size)) {
        ret = malloc(sizeof(size_t[1 + (2 * size) / 3]));
        n = numberline_inner(lbuf, ret, base);

        size_t len = n ? n : 1;
        ret = realloc(ret, sizeof(size_t[len]));
    }

    if (np)
        *np = n;
    return ret;
}

/**
 * Read one text line of of most `size - 1` bytes
 */
char*
fgetline(size_t size, char s[restrict size], FILE* restrict stream) {
    /**
     * Guarantee that `s` is _always_ null terminated:
     *   - either by successful `fgets`
     *   - or by enforcing it to be an empty string
     */
    s[0] = 0;
    char* ret = fgets(s, size, stream);
    if (ret) {
        char* pos = strchr(s, '\n');
        if (pos) {
            *pos = 0;
        } else
            ret = 0;
    }
    return ret;
}

/**
 * Print a series of numbers `nums` into `buf`, using `printf` format,
 * separated by `sep` chars & terminated with newline;
 * Returns the number of chars printed to `buf`
 */
int
sprintnumbers(size_t tot, char buf[restrict tot],
              char const form[restrict static 1],
              char const sep[restrict static 1], size_t len,
              size_t nums[restrict len]) {
    char* p = buf; // next position in `buf`
    size_t const seplen = strlen(sep);
    if (len) {
        size_t i = 0;
        for (;;) {
            // sprintf
            //  - always ensures a `0` placed at the end
            //  - returns length of the string printed, excluding the `0`
            p += sprintf(p, form, nums[i]);
            ++i;
            if (i >= len)
                break;
            memcpy(p, sep, seplen);
            p += seplen;
        }
    }

    memcpy(p, "\n", 2);
    return (p - buf) + 1;
}

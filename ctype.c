#include <ctype.h>
#include <stdio.h>

int main() {
    char c = 'B';
    printf("lower case of %c is: %c.\n", c, tolower(c));

    char sym = '9';
    if (isdigit(sym))
        printf("%c is a digit.\n", sym);
    else
        printf("%c is NOT a digit.\n", sym);

    return 0;
}

#include <stdio.h>
#include <string.h>

void reverse(char[]);

int main() {
    char str[] = "?guess who am I..";
    printf("%s\n", str);
    reverse(str);
    printf("%s\n", str);
    return 0;
}

void reverse(char str[]) {
    char c;
    for (int i = 0, j = strlen(str) - 1; i < j; ++i, --j) {
        c = str[i];
        str[i] = str[j];
        str[j] = c;
    }
    return;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef HASHSIZE
#define HASHSIZE 128
#endif

struct list_node {
    struct list_node* next;
    char* name;
    char* definition;
};

struct list_node* hashtab[HASHSIZE];

unsigned hash(char*);
struct list_node* lookup(char*);
struct list_node* install(char*, char*);
char* str_dup(char*);

int
main(int argc, char** argv) {
    char* name1 = "Miko";
    char* defn1 = "Junyao";
    char* name2 = "Jovi";
    char* defn2 = "Guangchu";
    char* defn3 = "Guangchu Guy";

    struct list_node* node1 = install(name1, defn1);
    struct list_node* node2 = install(name2, defn2);
    struct list_node* node3 = install(name2, defn3);

    /** node1 = lookup(name1);
     * node2 = lookup(name2); */
    if (node1 != NULL)
        printf("%s is: %s\n", name1, node1->definition);
    if (node2 != NULL)
        printf("%s is: %s\n", name2, node2->definition);
    if (node3 != NULL)
        printf("%s is: %s\n", name2, node3->definition);

    return 0;
}

struct list_node*
install(char* name, char* definition) {
    struct list_node* node_ptr = NULL;
    unsigned hash_val;
    if ((node_ptr = lookup(name)) == NULL) {
        // need to install a new node
        node_ptr = (struct list_node*)malloc(sizeof(struct list_node));
        if (node_ptr == NULL || (node_ptr->name = str_dup(name)) == NULL) {
            return NULL;
        }
        hash_val = hash(name);
        node_ptr->next = NULL;
        hashtab[hash_val] = node_ptr;
    } else {
        free((void*)node_ptr->definition);
    }
    if ((node_ptr->definition = str_dup(definition)) == NULL) {
        return NULL;
    }

    return node_ptr;
}

struct list_node*
lookup(char* name) {
    struct list_node* node_ptr;
    for (node_ptr = hashtab[hash(name)]; node_ptr != NULL; node_ptr++) {
        if (strcmp(name, node_ptr->name) == 0) {
            return node_ptr;
        }
    }

    return NULL;
}

unsigned
hash(char* str) {
    unsigned hash_val;
    for (hash_val = 0; *str != '\0'; str++) {
        hash_val = *str + 31 * hash_val;
    }

    return hash_val % HASHSIZE;
}

char*
str_dup(char* str) {
    char* dup = (char*)malloc(strlen(str) + 1);
    if (dup != NULL) {
        strncpy(dup, str, strlen(str) + 1);
    }
    return dup;
}

#include <stdio.h>
#include <math.h>

int main() {
    printf("%f.\n", sqrt((double)6));
    // compiled with:
    // clang sqrt.c -o sqrt -lm
    return 0;
}

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#define ZERO_DEFAULT_3_ARGS_0(FUNC, _0, _1, _2, ...) FUNC(_0, _1, _2)
#define ZERO_DEFAULT_3_ARGS(...) ZERO_DEFAULT_3_ARGS_0(__VA_ARGS__, 0, 0, )

#define strtoul(...) ZERO_DEFAULT_3_ARGS(strtoul, __VA_ARGS__)
#define strtoull(...) ZERO_DEFAULT_3_ARGS(strtoull, __VA_ARGS__)
#define strtol(...) ZERO_DEFAULT_3_ARGS(strtol, __VA_ARGS__)

int
main(int argc, char* argv[argc + 1]) {
    if (argc < 2) {
        return EXIT_FAILURE;
    }
    size_t len = strtoul(argv[1], 0);
    printf("len is %zu\n", len);
}

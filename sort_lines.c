#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef MAX_NUM_LINES
#define MAX_NUM_LINES 4096
#endif

#ifndef MAX_LEN
#define MAX_LEN 999
#endif

int get_line(char *, int);     // max of size of one line
int read_lines(char *[], int); // max of number of lines
void quick_sort(char *[], int, int);
void write_lines(char *[], int);
void swap(char *[], int, int);

int main() {
    char *line_ptr[MAX_NUM_LINES];
    int num_of_lines = 0;
    if ((num_of_lines = read_lines(line_ptr, MAX_NUM_LINES)) >= 0) {
        quick_sort(line_ptr, 0, num_of_lines - 1);
        write_lines(line_ptr, num_of_lines);
        return 0;
    } else {
        printf("ERROR: input too large to be sorted!\n");
        return -1;
    }
}

int get_line(char *str, int limit) {
    // return length I suppose
    int c = 0;
    int pos = 0;
    while (pos < limit - 1 && (c = getchar()) != EOF && c != '\n') {
        str[pos++] = c;
    }
    if (c == '\n') {
        str[pos++] = c;
    }
    str[pos] = '\0';
    return pos;
}

int read_lines(char *line_ptr[], int max_num_lines) {
    int len = 0;
    char line[MAX_LEN];
    char *ptr;
    int num_of_lines = 0;
    while ((len = get_line(line, MAX_LEN)) > 0) {
        if (num_of_lines >= MAX_NUM_LINES ||
            (ptr = (char *)malloc(sizeof(char) * len)) == NULL) {
            return -1;
        } else {
            // a valid line is read
            line[len - 1] = '\0'; /* delete new line */
            /** strncpy(ptr, line, MAX_LEN); */
            strncpy(ptr, line, len);
            line_ptr[num_of_lines++] = ptr;
        }
    }

    return num_of_lines;
}

void write_lines(char *line_ptr[], int num_of_lines) {
    for (int i = 0; i < num_of_lines; ++i) {
        printf("%s\n", line_ptr[i]);
    }
}

void quick_sort(char *line_ptr[], int left, int right) {
    if (left >= right)
        return;
    int pivot = (left + right) / 2;

    swap(line_ptr, left, pivot);
    pivot = left;
    int pos = 0;
    for (pos = pivot + 1; pos <= right; ++pos) {
        if (strcmp(line_ptr[pos], line_ptr[left]) < 0) {
            swap(line_ptr, ++pivot, pos);
        }
    }
    swap(line_ptr, left, pivot);
    quick_sort(line_ptr, left, pivot - 1);
    quick_sort(line_ptr, pivot + 1, right);
}

void swap(char *line_ptr[], int l, int r) {
    char *tmp = line_ptr[l];
    line_ptr[l] = line_ptr[r];
    line_ptr[r] = tmp;
}

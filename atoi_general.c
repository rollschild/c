#include <stdio.h>

#include <ctype.h>

int atoi_general(char[]);

int main() {
    char str[] = "-2037";
    printf("%s is actually: %d\n", str, atoi_general(str));
    char str2[] = "+2037";
    printf("%s is actually: %d\n", str2, atoi_general(str2));
    char str3[] = "  -2037";
    printf("%s is actually: %d\n", str3, atoi_general(str3));
}

int atoi_general(char str[]) {
    int pos = 0;
    int value = 0;
    // skip white spaces
    for (; isspace(str[pos]); ++pos)
        ;

    int sign = (str[pos] == '-') ? -1 : 1;
    if (str[pos] == '+' || str[pos] == '-')
        ++pos;

    while (isdigit(str[pos])) {
        value = value * 10 + str[pos] - '0';
        ++pos;
    }

    return sign * value;
}

#include <stdio.h>

int main() {
    int c = 0;
    int white_count, other_count;
    white_count = other_count = 0;
    int digits[10];
    
    for (int i = 0; i < 10; ++i)
        digits[i] = 0;

    while ((c = getchar()) != EOF) {
        if (c >= '0' && c <= '9')
            ++digits[c - '0'];
        else if (c == '\n' || c == ' ' || c == '\t')
            ++white_count;
        else 
            ++other_count;
    }

    printf("digits: ");
    for (int j = 0; j < 10; ++j) 
        printf("%d ", digits[j]);
    printf("\n, %d white space, and %d other chars.\n", white_count, other_count);
}

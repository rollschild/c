#include <ctype.h>
#include <stdio.h>
#include <string.h>

#ifndef MAX_WORD_LEN
#define MAX_WORD_LEN 128
#endif

#ifndef MAX_BUFFER
#define MAX_BUFFER 1024
#endif

int buffer[MAX_BUFFER];
int buffer_pos = 0;

struct key {
    char *name;
    int count;
} keytab[] = {
    {"break", 0},    {"case", 0},    {"char", 0},  {"const", 0},
    {"continue", 0}, {"default", 0}, {"else", 0},  {"else if", 0},
    {"for", 0},      {"if", 0},      {"int", 0},   {"switch", 0},
    {"unsigned", 0}, {"void", 0},    {"while", 0},
};

#ifndef KEY_NUM
#define KEY_NUM (sizeof(keytab) / sizeof(struct key))
#endif

struct key *
binary_search(char *, struct key *, int);

int get_word(char *, int);

int main(int argc, char **argv) {
    char keyword[MAX_WORD_LEN];
    struct key *key_ptr;

    while ((get_word(keyword, MAX_WORD_LEN)) != EOF) {
        if (isalpha(keyword[0])) {
            if ((key_ptr = binary_search(keyword, keytab, KEY_NUM)) != NULL) {
                key_ptr->count++;
            }
        }
    }

    for (key_ptr = keytab; key_ptr < keytab + KEY_NUM; key_ptr++) {
        printf("%s: %d\n", key_ptr->name, key_ptr->count);
    }

    return 0;
}

struct key *
binary_search(char *word, struct key *keytab_ptr, int limit) {
    struct key *low = &keytab_ptr[0];
    struct key *high = &keytab_ptr[limit - 1];

    while (low <= high) {
        struct key *mid = low + (high - low) / 2;
        if (strcmp(word, mid->name) < 0) {
            high = --mid;
        } else if (strcmp(word, mid->name) > 0) {
            low = ++mid;
        } else {
            return mid;
        }
    }

    return NULL;
}

int get_word(char *word, int limit) {
    int get_char(void);
    void unget_char(int);
    int c;
    char *w = word;

    while (isspace(c = get_char()))
        ;

    // now c has some none-space value
    if (c != EOF) {
        *w++ = c;
    }

    if (!isalpha(c)) {
        *w = c;
        return c;
    }

    while (--limit > 0 && isalnum(c = get_char())) {
        *w++ = c;
    }

    *w = '\0';
    unget_char(c);

    return word[0];
}

int get_char(void) {
    return (buffer_pos > 0) ? buffer[--buffer_pos] : getchar();
}

void unget_char(int c) {
    if (buffer_pos >= MAX_BUFFER) {
        printf("Do you like overflow?\n");
    } else {
        buffer[buffer_pos++] = c;
    }
    return;
}

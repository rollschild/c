#include <stdio.h>

int main() {
    int c = 0, line_count = 0;
     while ((c = getchar()) != EOF)
         if (c == '\n')
             ++line_count;

     printf("%d lines in total.\n", line_count);
}

#include <stdio.h>

void squeeze(char[], char);

int main() {
    char str[] = "guangchu";
    char c = getchar();
    squeeze(str, c);
    printf("%s.\n", str);

    return 0;

}

void squeeze(char str[], char c) {
    int i, j;
    for (i = 0, j = 0; str[i] != '\0'; ++i) {
        if (str[i] != c)
            str[j++] = str[i];
    }

    str[j] = '\0';
}

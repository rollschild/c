#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#ifndef PERMS
#define PERMS 0666
#endif

void error(char*, ...);

int
main(int argc, char** argv) {
    int f_source; // file descriptor is just an int
    int f_target;
    int num;
    char buf[BUFSIZ]; // this is just a mystery to me...

    if (argc != 3) {
        error("Invalid arguments!\n");
    } else {
        if ((f_source = open(argv[2], O_RDONLY, 0)) == -1) {
            // error
            error("Error opening source file %s\n", argv[2]);
        }
        if ((f_target = creat(argv[1], PERMS)) == -1) {
            error("Error creating target file %s, with mode %03o\n", argv[1],
                  PERMS);
        }
        while ((num = read(f_source, buf, BUFSIZ)) > 0) {
            if (write(f_target, buf, num) != num) {
                error("Error copying to target file %s\n", argv[1]);
            }
        }
    }

    return 0;
}

void
error(char* fmt, ...) {
    va_list args;

    va_start(args, fmt);
    fprintf(stderr, "error: ");
    vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
    va_end(args);

    exit(-1);
}

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#ifndef MAX_TOKEN_LEN
#define MAX_TOKEN_LEN 1024
#endif

#ifndef MAX_BUFFER
#define MAX_BUFFER 1024
#endif

enum { NAME, PARENS, BRACKTS };

int get_token(void);
void dcl();
void direct_dcl();
int token_char;
int buffer[MAX_BUFFER];
int buffer_pos = 0;
char token[MAX_TOKEN_LEN];
char name[MAX_TOKEN_LEN];
char out[MAX_TOKEN_LEN];
char datatype[MAX_TOKEN_LEN];

int main(int argc, char **argv) {
    while (get_token() != EOF) {
        // get datatype
        int pos = 0;
        while (token[pos] != '\0') {
            datatype[pos] = token[pos];
            ++pos;
        }
        datatype[pos] = '\0';
        // the above should be equivalent to strncpy()
        out[0] = '\0';
        dcl(); // get declaration
        if (token_char != '\n') {
            printf("Syntax error!\n");
        }
        printf("%s: %s %s\n", name, out, datatype);
    }

    return 0;
}

void dcl(void) {
    int num_of_ptrs = 0;
    while ((token_char = get_token()) == '*') {
        ++num_of_ptrs;
    }
    // there's one more token fetched, which is not a '*'
    direct_dcl();
    while (num_of_ptrs-- > 0) {
        strncat(out, " pointer to", strlen(" pointer to") + 1);
    }

    return;
}

void direct_dcl(void) {
    // token_char has a value that's not read/compared yet
    if (token_char == '(') {
        dcl();
        if (token_char != ')') {
            printf("Error: Invalid input! Missing ')'\n");
            return;
        }
    } else if (token_char == NAME) {
        int pos = 0;
        while (token[pos] != '\0') {
            name[pos] = token[pos];
            ++pos;
        }
        name[pos] = '\0';
    } else {
        printf("Error: Invalid input! Expected: (dcl)\n");
        return;
    }

    while ((token_char = get_token()) == PARENS || token_char == BRACKTS) {
        if (token_char == PARENS) {
            strncat(out, " function returning",
                    strlen(" function returning") + 1);
        } else {
            int len = 0;
            while (token[len++] != '\0')
                ;
            strncat(out, " array", strlen(" array") + 1);
            strncat(out, token, len);
            strncat(out, " of", strlen(" of") + 1);
        }
    }
    return;
}

int get_token(void) {
    int c;
    int get_char(void);
    void unget_char(int);
    char *ptr = token;

    // skip white spaces
    while ((c = get_char()) == ' ' || c == '\t')
        ;

    if (c == '(') {
        if ((c = get_char()) == ')') {
            strncpy(token, "()", strlen("()") + 1);
            return token_char = PARENS;
        } else {
            unget_char(c);
            return '(';
        }
    } else if (c == '[') {
        // has to be an array
        for (*ptr++ = c; (*ptr++ = get_char()) != ']';) {
            ;
        }
        // now *ptr = ']'
        *ptr = '\0';
        return token_char = BRACKTS;
    } else if (isalpha(c)) {
        // name
        for (*ptr++ = c; isalnum(c = get_char());) {
            *ptr++ = c;
        }
        *ptr = '\0';
        unget_char(c);
        return token_char = NAME;
    } else {
        return token_char = c;
    }
}

int get_char(void) {
    return (buffer_pos > 0) ? buffer[--buffer_pos] : getchar();
}

void unget_char(int c) {
    if (buffer_pos >= MAX_BUFFER) {
        printf("too many inputs!\n");
    } else {
        buffer[buffer_pos++] = c;
    }
}


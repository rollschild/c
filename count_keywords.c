#include <ctype.h>
#include <stdio.h>
#include <string.h>

#ifndef MAX_WORD_LEN
#define MAX_WORD_LEN 128
#endif

#ifndef MAX_BUFFER
#define MAX_BUFFER 1024
#endif

int buffer[MAX_BUFFER];
int buffer_pos = 0;

struct key {
    char *name;
    int count;
} keytab[] = {
    {"break", 0},    {"case", 0},    {"char", 0},  {"const", 0},
    {"continue", 0}, {"default", 0}, {"else", 0},  {"else if", 0},
    {"for", 0},      {"if", 0},      {"int", 0},   {"switch", 0},
    {"unsigned", 0}, {"void", 0},    {"while", 0},
};

#ifndef KEY_NUM
#define KEY_NUM (sizeof(keytab) / sizeof(struct key))
#endif

int get_word(char *, int);
int binary_search(char *, struct key *, int);

int main(int argc, char **argv) {
    char word[MAX_WORD_LEN];
    int pos;

    while (get_word(word, MAX_WORD_LEN) != EOF) {
        // a word is read
        if (isalpha(word[0]))
            if ((pos = binary_search(word, keytab, KEY_NUM)) >= 0) {
                keytab[pos].count++;
            }
    }

    for (int i = 0; i < KEY_NUM; ++i) {
        printf("keyword: %s\tcount: %d\n", keytab[i].name, keytab[i].count);
    }

    return 0;
}

int binary_search(char *key_name, struct key key_array[], int array_size) {
    int low = 0;
    int high = array_size - 1;
    int mid;

    while (low <= high) {
        mid = (low + high) / 2;
        if (strcmp(key_name, key_array[mid].name) < 0) {
            high = mid - 1;
        } else if (strcmp(key_name, key_array[mid].name) > 0) {
            low = mid + 1;
        } else {
            return mid;
        }
    }

    return -1;
}

int get_word(char *word, int limit) {

    int c;
    char *w = word;
    int get_char(void);
    void unget_char(int);

    while (isspace(c = get_char()))
        ;

    if (c != EOF) {
        *w++ = c;
    }
    if (!isalpha(c)) {
        *w = '\0';
        return c;
    }

    // now c is alpha
    while (--limit > 0 && isalnum(c = get_char())) {
        *w++ = c;
    }
    unget_char(c);
    *w = '\0';

    return word[0];
}

int get_char(void) {
    return (buffer_pos > 0) ? buffer[--buffer_pos] : getchar();
}

void unget_char(int c) {
    if (buffer_pos >= MAX_BUFFER) {
        printf("Overflow!\n");
    } else {
        buffer[buffer_pos++] = c;
    }
    return;
}

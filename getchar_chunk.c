#include <stdio.h>
#include <unistd.h>

#undef getchar

int
getchar(void) {
    static char buf[BUFSIZ];
    static char* buf_ptr = buf;
    static int num;

    if (num == 0) {
        printf("buf before read-in: %p\n", buf);
        num = read(0, buf, sizeof(buf));
        printf("buf after read-in: %p\n", buf);
        buf_ptr = buf;
    }

    return (--num >= 0) ? (unsigned char)*buf_ptr++ : EOF;
}

int
main(int argc, char** argv) {
    char c1 = getchar();
    char c2 = getchar();

    printf("c1: %c\n", c1);
    printf("c2: %c\n", c2);

    return 0;
}

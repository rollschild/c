#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#ifndef MAX_NUM_LINES
#define MAX_NUM_LINES 1024
#endif

#ifndef MAX_LEN
#define MAX_LEN 256
#endif

int get_line(char *, int);

int main(int argc, char **argv) {
    long num_of_lines = 0;
    int c = 0;
    int len = 0;
    int found = 0;
    char line[MAX_LEN];
    bool except = false;
    bool number = false;

    while (--argc > 0 && (*++argv)[0] == '-') {
        while ((c = *(++argv[0]))) {
            switch (c) {
            case 'x':
                except = true;
                break;
            case 'n':
                number = true;
                break;
            default:
                printf("wrong handle!\n");
                argc = 0;
                found = -1;
                break;
            }
        }
    }

    if (argc != 1) {
        printf("wrong number of arguments my guy!\n");
    } else {
        while ((len = get_line(line, MAX_LEN)) > 0) {
            num_of_lines++;
            if ((strstr(line, *argv) != NULL) != except) {
                if (number) {
                    printf("%ld: ", num_of_lines);
                }
                printf("%s", line);
                ++found;
            }
        }
    }

    return found;
}

int get_line(char *line, int limit) {
    int pos = 0;
    int c = 0;
    while (pos < limit - 1 && (c = getchar()) != EOF && c != '\n') {
        line[pos++] = c;
    }
    if (c == '\n') {
        line[pos++] = c;
    }
    line[pos] = '\0';

    return pos;
}

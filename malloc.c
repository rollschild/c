#include <stdio.h>
#include <string.h>

#ifdef malloc_impl
#undef malloc_impl
#endif

#ifndef NUM_ALLOC
#define NUM_ALLOC 1024
#endif

typedef long Align;

union header {
    struct {
        union header* next;
        unsigned size;
    } info;
    Align alignment;
};

typedef union header Header;

static Header base;
static Header* free_ptr = NULL;

static Header* morecore_impl(unsigned);

void*
malloc_impl(unsigned num_of_bytes) {
    Header *ptr, *prev_ptr;
    /** Header* morecore_impl(unsigned); */
    unsigned num_of_units =
        (num_of_bytes + sizeof(Header) - 1) / sizeof(Header) + 1;

    if ((prev_ptr = free_ptr) == NULL) {
        // no free list available
        base.info.size = 0;
        base.info.next = free_ptr = prev_ptr = &base;
    }

    for (ptr = prev_ptr->info.next;; prev_ptr = ptr, ptr = ptr->info.next) {
        if (ptr->info.size >= num_of_units) {
            if (ptr->info.size == num_of_units)
                prev_ptr->info.next = ptr->info.next;
            else {
                /* tail end */
                ptr->info.size -= num_of_units;
                ptr += ptr->info.size;
                ptr->info.size = num_of_units;
            }
            free_ptr = prev_ptr;
            return (void*)(ptr + 1);
        }
        if (ptr == free_ptr) {
            // wrapped around free list
            if ((ptr = morecore_impl(num_of_units)) == NULL) {
                return NULL;
            }
        }
    }
}

static Header*
morecore_impl(unsigned num_of_units) {
    // ask system for more memory
    void free_impl(void*);
    Header* ptr;
    char *cp, *sbrk(int);
    num_of_units = (num_of_units < NUM_ALLOC) ? NUM_ALLOC : num_of_units;
    cp = sbrk(num_of_units * sizeof(Header));
    if (cp == (char*)-1) {
        return NULL;
    }
    ptr = (Header*)cp;
    ptr->info.size = num_of_units;
    free_impl((void*)(ptr + 1));
    return free_ptr;
}

void
free_impl(void* target_ptr) {
    Header *block_ptr, *ptr;

    block_ptr = (Header*)target_ptr - 1; // point to block header
    for (ptr = free_ptr; !(block_ptr > ptr && block_ptr < ptr->info.next);
         ptr = ptr->info.next) {
        if (ptr >= ptr->info.next &&
            (block_ptr > ptr || block_ptr < ptr->info.next)) {
            break;
        }
    }
    if (block_ptr + block_ptr->info.size == ptr->info.next) {
        block_ptr->info.size += ptr->info.next->info.size;
        block_ptr->info.next = ptr->info.next->info.next;
    } else {
        block_ptr->info.next = ptr->info.next;
    }
    if (ptr + ptr->info.size == block_ptr) {
        ptr->info.size += block_ptr->info.size;
        ptr->info.next = block_ptr->info.next;
    } else {
        ptr->info.next = block_ptr;
    }

    free_ptr = ptr;
}

int
main() {
    /** char* str = "hello"; */
    char* str = (char*)malloc_impl(sizeof(char) * 6);
    /** char* str; */
    str[0] = 'h';
    str[1] = 'e';
    str[2] = 'l';
    str[3] = 'l';
    str[4] = 'o';
    str[5] = '\0';

    printf("4th of %s is: %c\n", str, str[4]);

    return 0;
}

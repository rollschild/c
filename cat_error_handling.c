#include <stdio.h>
#include <stdlib.h>

void filecopy(FILE*, FILE*);

int
main(int argc, char** argv) {
    FILE* fp;
    char* prog = argv[0];

    if (argc <= 1) {
        filecopy(stdout, stdin);
    } else {
        while (--argc > 0) {
            if ((fp = fopen(*++argv, "r")) == NULL) {
                fprintf(stderr, "%s: cannot open file %s\n", prog, *argv);
                exit(-1);
            } else {
                filecopy(stdout, fp);
                fclose(fp);
            }
        }
    }

    if (ferror(stdout)) {
        fprintf(stderr, "%s: Error writing to output!\n", prog);
        exit(-2);
    }

    exit(0);
}

void
filecopy(FILE* target, FILE* source) {
    int c;

    while ((c = getc(source)) != EOF) {
        putc(c, target);
    }

    return;
}

#include <stdio.h>

void filecopy(FILE*, FILE*);

int
main(int argc, char** argv) {
    FILE* fp;

    if (argc <= 1) {
        filecopy(stdout, stdin);
    } else {
        // a file name specified
        while (--argc > 0) {
            if ((fp = fopen(*++argv, "r")) == NULL) {
                printf("Invalid file name! Cannot read it.\n");
                return -1;
            } else {
                filecopy(stdout, fp);
                fclose(fp);
            }
        }
    }

    return 0;
}

void
filecopy(FILE* target, FILE* source) {
    int c;
    while ((c = getc(source)) != EOF) {
        putc(c, target);
    }

    return;
}

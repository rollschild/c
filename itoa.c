#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void reverse(char* str) {
    int len = strlen(str) - 1;
    if (len < 0) return;
    for (int i = 0; i < len; ++i, --len) {
        char c = str[i];
        str[i] = str[len];
        str[len] = c;
    }
    return;
}

char* itoa(int number) {
    int sign;
    if ((sign = number) < 0) {
        number = -number;
    }
    int pos = 0;
    char* res = malloc(255 * sizeof(char));
    do {
        res[pos++] = number % 10 + '0';
    } while ((number /= 10) > 0);

    if (sign < 0) {
        res[pos++] = '-';
    }
    res[pos] = '\0';

    reverse(res);
    return res;
}

int main() {
    /** int num = -2037; */
    int num = 0;
    printf("%s\n", itoa(num));
    return 0;
}

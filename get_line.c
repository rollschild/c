#include <stdio.h>

#define MAX_LEN 1000

// Assume NO overflow for now

int get_newline(char line[], int limit);
void copy(char to[], char from[]);

int main() {
    int len = 0;
    int max = 0;
    char line[MAX_LEN];
    char longest[MAX_LEN];

    while ((len = get_newline(line, MAX_LEN)) > 0) {
        if (len > max) {
            max = len;
            copy(longest, line);
        }
    }

    printf("Line with max length is: \n");
    if (max > 0) {
        /** for (int i = 0; i < max - 1; ++i)
         *     printf("%c", longest[i]); */
        printf("%s", longest);
        printf("and length is %d.\n", max);
    }
    return 0;
}

int get_newline(char tmp[], int limit) {
    int c = 0;
    int i;
    for (i = 0; i < limit - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        tmp[i] = c;
    }
    if (c == '\n') {
        tmp[i++] = c;
    }
    tmp[i] = '\0';

    return i;
}

void copy(char to[], char from[]) {
    int pos = 0;

    while ((to[pos] = from[pos]) != '\0')
        ++pos;

    return;
}

#include <stdio.h>

#ifndef MAXLEN
#define MAXLEN 256
#endif

int get_newline(char*, int);

int
main(int argc, char** argv) {
    int day, month, year;
    char line[MAXLEN];
    char monthname[12];

    while (get_newline(line, sizeof(line)) > 0) {
        if (sscanf(line, "%d %s %d", &day, monthname, &year) == 3) {
            printf("valid: %s\n", line); /* 4 June 1989 */
        } else if (sscanf(line, "%d/%d/%d", &month, &day, &year) == 3) {
            printf("valid: %s\n", line); /* mm/dd/yy */
        } else {
            printf("invalid: %s\n", line);
        }
    }

    return 0;
}

int
get_newline(char tmp[], int limit) {
    int c = 0;
    int i;
    for (i = 0; i < limit - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        tmp[i] = c;
    }
    if (c == '\n') {
        tmp[i++] = c;
    }
    tmp[i] = '\0';

    return i;
}

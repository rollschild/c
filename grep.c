#include <stdio.h>
#include <string.h>

#ifndef MAX_LEN
#define MAX_LEN 999
#endif

int get_line(char *, int);

int main(int argc, char **argv) {
    char line[MAX_LEN];
    int len = 0;
    int found = 0;

    if (argc != 2) {
        printf("Usage: please specify one argument!\n");
    } else {
        while ((len = get_line(line, MAX_LEN)) > 0) {
            if (strstr(line, argv[1]) != NULL) {
                printf("%s", line);
                ++found;
            }
        }
    }

    printf("pattern %s occurs %d times in total!\n", argv[1], found);
    return found;
}

int get_line(char *line, int limit) {
    int c = 0;
    int pos = 0;
    while (pos < limit - 1 && (c = getchar()) != EOF && c != '\n') {
        line[pos++] = c;
    }
    if (c == '\n') {
        line[pos++] = c;
    }
    line[pos] = '\0';

    return pos;
}

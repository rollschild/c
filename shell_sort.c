#include <stdio.h>

void shell_sort(int[], int);

int main() {
    int arr[] = {12, 0, -1, 18, 9, 6};

    for (int i = 0; i < 6; ++i)
        printf("%d ", arr[i]);
    printf("\n");

    shell_sort(arr, 6);
    for (int i = 0; i < 6; ++i)
        printf("%d ", arr[i]);
    printf("\n");

    return 0;
}

void shell_sort(int arr[], int len) {
    for (int gap = len / 2; gap > 0; gap /= 2) {
        for (int pioneer = gap; pioneer < len; ++pioneer) {
            for (int trailer = pioneer - gap;
                 trailer >= 0 && arr[trailer] > arr[trailer + gap];
                 trailer -= gap) {
                int temp = arr[trailer + gap];
                arr[trailer + gap] = arr[trailer];
                arr[trailer] = temp;
            }
        }
    }
    return;
}

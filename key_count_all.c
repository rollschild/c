#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef MAX_WORD_COUNT
#define MAX_WORD_COUNT 1024
#endif

#ifndef MAX_LEN
#define MAX_LEN 256
#endif

struct node {
    char* name;
    int count;
    struct node* left;
    struct node* right;
};

int buffer[MAX_WORD_COUNT];
int buffer_pos = 0;
int get_word(char*, int);
struct node* add_to_tree(struct node*, char*);
void print_tree(struct node*);
char* str_dup(char*);

int
main(int argc, char **argv) {
    char word[MAX_LEN];
    int c;
    struct node *root = NULL;

    while ((c = get_word(word, MAX_LEN)) != EOF) {
        if (isalpha(word[0])) {
            root = add_to_tree(root, word);
        }
    }

    print_tree(root);

    return 0;
}

struct node*
add_to_tree(struct node* root, char* word) {
    if (root == NULL) {
        root = (struct node*)malloc(sizeof(struct node));
        if (root != NULL) {
            root->name = str_dup(word);
            root->count = 1;
            root->left = root->right = NULL;
        }
    } else if (strcmp(root->name, word) < 0) {
        // add to right child
        root->right = add_to_tree(root->right, word);
    } else if (strcmp(root->name, word) > 0) {
        // add to left child
        root->left = add_to_tree(root->left, word);
    } else {
        // right here!
        ++root->count;
    }

    return root;
}

void
print_tree(struct node* root) {
    if (root != NULL) {
        print_tree(root->left);
        printf("word: %s\tcount: %d\n", root->name, root->count);
        print_tree(root->right);
    }
    return;
}

char*
str_dup(char* word) {
    char* dup = (char*)malloc(strlen(word) + 1);
    if (dup != NULL) {
        dup = strncpy(dup, word, strlen(word) + 1);
    }
    return dup;
}

int
get_word(char* word, int limit) {
    int get_char(void);
    void unget_char(int);

    char* word_ptr = word;
    int c;

    while (isspace(c = get_char()))
        ;

    if (c != EOF) {
        *word_ptr++ = c;
    }
    if (!isalpha(c)) {
        *word_ptr = '\0';
        return c;
    }

    while (--limit > 0 && isalnum(c = get_char())) {
        // valid char read in
        *word_ptr++ = c;
    }

    unget_char(c);
    *word_ptr = '\0';

    return word[0];
}

int
get_char(void) {
    return (buffer_pos > 0) ? buffer[--buffer_pos] : getchar();
}

void
unget_char(int c) {
    if (buffer_pos >= MAX_WORD_COUNT)
        printf("Full!\n");
    else
        buffer[buffer_pos++] = c;
    return;
}
